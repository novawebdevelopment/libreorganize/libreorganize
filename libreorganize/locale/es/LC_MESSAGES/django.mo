��    .      �  =   �      �     �     �           	                    "  	   *  	   4  	   >  	   H     R     Y     k     r     w          �     �     �     �     �     �     �  	   �     �     �     �     �     �     �  
           '     /   C  +   s     �     �  0   �     �            S     (   `     �     �     �  
   �     �     �     �     �     �  	   �  	   �  	      	   
               7     @     G  	   Y     c  	   o     y     �     �  	   �     �     �     �     �  	   �     �     �     �     �     	  4   "	  8   W	  .   �	     �	     �	  0   �	     
     &
     +
  J   1
  "   |
           	                                *          +   %   "          #       '       ,                 !          
   &             -   (                           )               $                        .       Accounts Boxes Calendar Cancel Cash Continue Dr. English Error 400 Error 403 Error 404 Error 500 Events External Redirect Female Home Join us Male Memberships Miss Mr. Mrs. Ms. Newsletters Normal - $10.00 One Month One Year Other Polls Prof. Ranked Choice Regular Voting Report bug Request feature Sorry! An error has occured on our end. Sorry! We couldn't find the page you requested. Sorry! We couldn't understand your request. Spanish Student - $5.00 The {self.model.__name__.lower()} doesn't exist. Welcome to the Dashboard Wiki Wikis You are being redirected to an external website. Are you sure you want to continue? You don't have the required permissions. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-06 23:59-0400
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Cuentas Cajas Calendario Cancelar Efectivo Seguir Doctor/a Ingles Error 400 Error 403 Error 404 Error 500 Eventos Redireccionamiento Externo Femenino Inicio Únete a Nosotros Masculino Membresías Señorita Señor Señora Sra. Boletines Normal - $10.00 Un Mes Un Año Otro Encuestas Prof. Elección Clasificada Votación Regular Reportar un error Característica de solicitud ¡Lo siento! Ha ocurrido un error por nuestra parte. ¡Lo siento! No pudimos encontrar la página solicitada. ¡Lo siento! No pudimos entender su solicitud. Español Estudiante - $5.00 El {self.model .__ name __. Lower ()} no existe. Bienvenido al Salpicadero Wiki Wikis Se le redirige a un sitio web externo. Estás seguro que quieres ¿Seguir? No tienes los permisos necesarios. 