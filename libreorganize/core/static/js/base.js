$(document).scroll(function () {
    $(".navbar").toggleClass("scrolled", $(this).scrollTop() > 0 || $(".navbar-collapse").hasClass('show'));
});

$(document).ready(function () {
    setTimeout(function () {
        $(".alert-message").alert("close");
    }, 5000);
    $('[data-toggle="tooltip"]').tooltip();
    table = $('#jsTable').DataTable({
        responsive: true,
        dom: '<"row no-gutters flex-nowrap"<"flex-grow-1"f><"flex-shrink-0 ml-1"l>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        lengthMenu: [5, 10, 25, 50, 100],
        pagingType: "numbers",
        pageLength: 10,
        aaSorting: [],
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search"
        },
        columnDefs: [{
            targets: [-1],
            orderable: false
        },
        {
            targets: [0, -1],
            responsivePriority: 1
        }]
    });
    $('#jsTable_length').removeClass('dataTables_length');
    $('#jsTable_filter').removeClass('dataTables_filter');
    $('#jsTable_filter > label').contents().unwrap();
    $('#jsTable_length > label').contents().unwrap();
    $('#jsTable_filter > input').removeClass("form-control-sm");
    $('#jsTable_length > select').removeClass("custom-select-sm form-control-sm");
    checkPermissions();
    $('#id_is_superuser').on("change", checkPermissions);
    $('.navbar-collapse').on('show.bs.collapse', function () {
        $(".navbar").addClass('scrolled');
    });
    $('.navbar-collapse').on('hide.bs.collapse', function () {
        $(".navbar").toggleClass("scrolled", $(document).scrollTop() > 0);
    });
});

$(function () {
    $("#open-dashboard").click(function () {
        document.getElementById("dashboard").classList.add("visible");
        $("body").css("overflow", "hidden");
    });
    $("#close-dashboard").click(function () {
        document.getElementById("dashboard").classList.remove("visible");
        $("body").css("overflow", "auto");
    });
});

function checkPermissions() {
    if (document.getElementById('div_id_user_permissions') != null) {
        $('#div_id_user_permissions > div > div > input').prop('disabled', document.getElementById('id_is_superuser').checked);
    }
}
