import os

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django.views.generic import TemplateView

urlpatterns = []

if os.path.exists(os.path.join(settings.BASE_DIR, "theme")):
    from theme.urls import *

urlpatterns += [
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
    path("accounts/", include("apps.accounts.urls", namespace="accounts")),
    path("boxes/", include("apps.boxes.urls", namespace="boxes")),
    path("events/", include("apps.events.urls", namespace="events")),
    path("news/", include("apps.news.urls", namespace="news")),
    path("votations/", include("apps.votations.urls", namespace="votations")),
    path("wikis/", include("apps.wikis.urls", namespace="wikis")),
    path("i18n/", include("django.conf.urls.i18n")),
    path("ckeditor/", include("ckeditor_uploader.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
