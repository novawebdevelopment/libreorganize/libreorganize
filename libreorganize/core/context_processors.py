from django.conf import settings


def constants(request):
    return {"WEBSITE_NAME": settings.WEBSITE_NAME, "CAN_REGISTER": settings.CAN_REGISTER}
