from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from django.utils.translation import gettext_lazy as _
from django.views import View

from core.mixins import ModelMixin, PermissionMixin, GroupPermissionMixin, RedirectMixin


class CoreListView(PermissionMixin, View):
    personal = None
    permission = None
    model = None
    template_name = None

    def get(self, request):
        return render(request=request, template_name=self.template_name, context=self.get_context())

    def get_context(self):
        if self.model.__name__.lower() != "account" and hasattr(self.model, "groups"):
            instances = self.model.objects.filter(groups=None)
            if self.request.user.is_superuser:
                instances = self.model.objects.all()
            else:
                for group in self.request.user.groups.all():
                    instances = instances | self.model.objects.filter(groups=group)
                instances = instances.distinct()
        else:
            instances = self.model.objects.all()
        return {f"{self.model.__name__.lower()}_list": instances}


class CoreDetailView(PermissionMixin, ModelMixin, GroupPermissionMixin, View):
    personal = None
    permission = None
    model = None
    template_name = None

    def get(self, request):
        return render(request=request, template_name=self.template_name, context=self.get_context())

    def get_context(self):
        return {self.model.__name__.lower(): getattr(self, self.model.__name__.lower())}


class CoreCreateView(PermissionMixin, RedirectMixin, View):
    personal = None
    permission = None
    model = None
    form_name = None
    template_name = None

    def get(self, request):
        return render(request=request, template_name=self.template_name, context=self.get_context())

    def post(self, request):
        context = self.get_context()
        if context["form"].is_valid():
            context["form"].save()
            messages.success(request, _(f"The {self.model.__name__.lower()} has been successfully created."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name=self.template_name, context=context)

    def get_context(self):
        if self.request.method == "POST":
            form = self.form_name(data=self.request.POST, files=self.request.FILES)
        else:
            form = self.form_name()

        if form.fields.get("groups") and not self.request.user.is_superuser:
            form.fields.pop("groups")

        return {"form": form}


class CoreEditView(PermissionMixin, ModelMixin, GroupPermissionMixin, RedirectMixin, View):
    personal = None
    permission = None
    model = None
    form_name = None
    template_name = None

    def get(self, request):
        return render(request=request, template_name=self.template_name, context=self.get_context())

    def post(self, request):
        context = self.get_context()
        if context["form"].is_valid():
            context["form"].save()
            messages.success(request, _(f"The {self.model.__name__.lower()} has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name=self.template_name, context=context)

    def get_context(self):
        if self.request.method == "POST":
            form = self.form_name(
                data=self.request.POST, files=self.request.FILES, instance=getattr(self, self.model.__name__.lower())
            )
        else:
            form = self.form_name(instance=getattr(self, self.model.__name__.lower()))

        if form.fields.get("groups") and not self.request.user.is_superuser:
            form.fields.pop("groups")

        return {"form": form}


class CoreDeleteView(PermissionMixin, ModelMixin, GroupPermissionMixin, RedirectMixin, View):
    personal = None
    permission = None
    model = None
    template_name = None

    def get(self, request):
        return render(request=request, template_name=self.template_name, context=self.get_context())

    def post(self, request):
        getattr(self, self.model.__name__.lower()).delete()
        messages.success(request, _(f"The {self.model.__name__.lower()} has been successfully deleted."))
        return HttpResponseRedirect(self.next)

    def get_context(self):
        return {}