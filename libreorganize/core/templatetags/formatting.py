from django import template

register = template.Library()


@register.filter(is_safe=True)
def format_phone(value):
    if len(value) == 10:
        return "(" + value[:3] + ") " + value[3:6] + "-" + value[6:10]
    elif (len(value) == 12 or len(value) == 13) and value[0] == "+":
        return value[:-10] + " (" + value[-10:-7] + ") " + value[-7:-4] + "-" + value[-4:]
    else:
        return value