import os

from django.contrib.messages import constants
from django.utils.translation import gettext_lazy as _

# ---------------------------------------------------------------------
# CORE
# ---------------------------------------------------------------------

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = "8=spx6&7hxk_3+jr^9^4oz4demzqpw9&lk!504r9gyq(2cyx(s"
WEBSITE_NAME = "LibreOrganize"
ALLOWED_HOSTS = ("*",)
DEBUG = True

AUTH_USER_MODEL = "accounts.Account"
ROOT_URLCONF = "core.urls"
WSGI_APPLICATION = "core.wsgi.application"

COMPRESS_PRECOMPILERS = (("text/x-scss", "django_libsass.SassCompiler"),)

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

if DEBUG:
    STATIC_ROOT = os.path.join(BASE_DIR, "static")
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
else:
    STATIC_ROOT = "/var/www/libreorganize/static/"
    MEDIA_ROOT = "/var/www/libreorganize/media/"

# ---------------------------------------------------------------------
# DATABASE
# ---------------------------------------------------------------------

if DEBUG:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": "libreorganize.sqlite3",
        }
    }
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": "libreorganize",
            "USER": "libreorganize",
            "PASSWORD": "libreorganize",
            "HOST": "localhost",
            "PORT": "",
        }
    }

# prevents new warnings in django 3.2
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# ---------------------------------------------------------------------
# INTERNATIONALIZATION
# ---------------------------------------------------------------------

LANGUAGE_CODE = "en"
TIME_ZONE = "UTC"
DATE_FORMAT = "M d, Y"
DATETIME_FORMAT = "M d, Y g:i A"
FIRST_WEEKDAY = 6

USE_I18N = True
USE_L10N = False
USE_TZ = False

# ---------------------------------------------------------------------
# ACCOUNTS
# ---------------------------------------------------------------------

CAN_REGISTER = True

# ---------------------------------------------------------------------
# EVENTS
# ---------------------------------------------------------------------


ICAL_PRODID = f"-//FQDN//LibreOrganize Calendar//{LANGUAGE_CODE.upper()}"
ICAL_FILENAME = "libreorganize.ics"


# ---------------------------------------------------------------------
# EMAIL
# ---------------------------------------------------------------------

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST = ""
EMAIL_PORT = 587
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
EMAIL_USE_TLS = True

# ---------------------------------------------------------------------
# CRISPY FORMS
# ---------------------------------------------------------------------

CRISPY_TEMPLATE_PACK = "bootstrap4"
MESSAGE_TAGS = {
    constants.DEBUG: "debug",
    constants.INFO: "info",
    constants.SUCCESS: "success",
    constants.WARNING: "warning",
    constants.ERROR: "danger",
}

# ---------------------------------------------------------------------
# MISCELLANEOUS
# ---------------------------------------------------------------------

NAME_REGEX = r"^[A-Za-zÁÉÍÓÚÑÜáéíóúñü\.\,\'\-\ ]+$"
PASSWORD_REGEX = r"^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$"
PASSWORD_RESET_TIMEOUT_DAYS = 1

# ---------------------------------------------------------------------
# PACKAGES
# ---------------------------------------------------------------------

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # Third-Party Apps
    "django_countries",
    "crispy_forms",
    "tempus_dominus",
    "ckeditor",
    "compressor",
    "crispy_bootstrap4",
    # LibreOrganize Apps
    "core",
    "apps.accounts",
    "apps.boxes",
    "apps.events",
    "apps.news",
    "apps.votations",
    "apps.wikis",
    "django_cleanup.apps.CleanupConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

if not DEBUG:
    MIDDLEWARE.insert(0, "django.middleware.cache.UpdateCacheMiddleware")
    MIDDLEWARE.append("django.middleware.cache.FetchFromCacheMiddleware")

# ---------------------------------------------------------------------
# TEMPLATES
# ---------------------------------------------------------------------

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.constants",
            ],
        },
    },
]

# ---------------------------------------------------------------------
#
# ---------------------------------------------------------------------

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "cache",
    }
}

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]


# ---------------------------------------------------------------------
#  CKEditor5
# ---------------------------------------------------------------------

CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_CONFIGS = {
    "default": {
        "width": "100%",
    }
}

# special field names
TOTAL_FORM_COUNT = "TOTAL_FORMS"
INITIAL_FORM_COUNT = "INITIAL_FORMS"
MIN_NUM_FORM_COUNT = "MIN_NUM_FORMS"
MAX_NUM_FORM_COUNT = "MAX_NUM_FORMS"
ORDERING_FIELD_NAME = "ORDER"
DELETION_FIELD_NAME = "DELETE"

# default minimum number of forms in a formset
DEFAULT_MIN_NUM = 1

# default maximum number of forms in a formset, to prevent memory exhaustion
DEFAULT_MAX_NUM = 10


# ---------------------------------------------------------------------
#  CHOICES
# ---------------------------------------------------------------------

FREQUENCY_CHOICES = (
    ("weekly", "Weekly"),
    ("monthly", "Monthly"),
    ("yearly", "Yearly"),
)

LANGUAGES = (("en", _("English")), ("es", _("Spanish")))

TYPE_VOTING = (("ranked", _("Ranked Choice")), ("normal", _("Regular Voting")))

if os.path.exists(os.path.join(BASE_DIR, "theme")):
    from theme.settings import *

    LOCALE_PATHS = (os.path.join(BASE_DIR, "theme", "locale"),)
    STATICFILES_DIRS = (os.path.join(BASE_DIR, "theme", "static"),)
    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [os.path.join(BASE_DIR, "theme", "templates")],
            "APP_DIRS": True,
            "OPTIONS": {
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                    "core.context_processors.constants",
                ],
            },
        },
    ]

TEMPLATES[0]["OPTIONS"]["context_processors"].append("apps.events.context_processors.events_processor")
TEMPLATES[0]["OPTIONS"]["context_processors"].append("apps.news.context_processors.announcements_processor")
