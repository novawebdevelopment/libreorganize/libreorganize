from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django.utils.translation import gettext_lazy as _


class PermissionMixin:
    """
    Verify that the user has the required permissions to access the view

    Attributes:
        personal        Let the user access the view without the permission (if it is related to them)
        permission      Users require the permission to access the view
                        - Set to False to allow logged out users to access
                        - Set to True to require users to be logged in
                        - Set to superuser to allow only superusers
                        - Set to a permission to require users to be logged in and have that permission
    """

    personal = None
    permission = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.personal is None:
            raise ImproperlyConfigured("PermissionMixin does not allow personal = None")
        if self.permission is None:
            raise ImproperlyConfigured("PermissionMixin does not allow permission = None")

    def dispatch(self, request, *args, **kwargs):
        # Is it required for the user to be logged in?
        if self.permission is False:
            return super().dispatch(request, *args, **kwargs)
        # Yes, send them to the login page.
        if not request.user.is_authenticated:
            return HttpResponseRedirect("/accounts/login/?next=" + request.get_full_path())
        # Are they trying to check their own account?
        if self.personal and self.model.__name__.lower() == "account":
            if request.user.pk == kwargs.get("pk", -1):
                return super().dispatch(request, *args, **kwargs)
        # No. Do they just have to be logged in?
        if self.permission is True:
            return super().dispatch(request, *args, **kwargs)
        # No. Do they have the required permission?
        if request.user.has_perm(self.permission):
            # Yes. Is there a group associated with what they are trying to access?
            if False:
                pass
            # No. Let them in.
            else:
                return super().dispatch(request, *args, **kwargs)
        # No. Is this a superuser-only view?
        if self.permission == "superuser" and request.user.is_superuser is True:
            return super().dispatch(request, *args, **kwargs)
        # They don't have the required permissions.
        messages.error(request, _("You don't have the required permissions."))
        return HttpResponseRedirect(reverse("home"))


class ModelMixin:
    """
    Get an instance of the specified model (expects a PK in the URL)

    Attributes:
        model           The model to get the instance of
    """

    model = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.model is None:
            raise ImproperlyConfigured("ModelMixin does not allow model = None")

    def dispatch(self, request, *args, **kwargs):
        try:
            # Did we pass a primary key in the URL?
            if kwargs.get("pk"):
                setattr(self, self.model.__name__.lower(), self.model.objects.get(pk=kwargs.get("pk")))
            # No. Did we pass a slug in the URL?
            else:
                setattr(self, self.model.__name__.lower(), self.model.objects.get(slug=kwargs.get("slug")))
        # We did not find a model with that PK / slug.
        except (TypeError, ValueError, OverflowError, self.model.DoesNotExist):
            messages.error(request, _(f"The {self.model.__name__.lower()} doesn't exist."))
            return HttpResponseRedirect(reverse("home"))
        return super().dispatch(request)


class GroupPermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        # Does this model support groups?
        if self.model.__name__.lower() != "account" and hasattr(self.model, "groups"):
            # Is the user a superuser?
            if self.request.user.is_superuser:
                return super().dispatch(request, *args, **kwargs)
            # No. Does the model have any groups associated?
            instance = getattr(self, self.model.__name__.lower())
            if instance.groups.count():
                # Check if any groups the user is part of match one of the groups associated.
                for group in self.request.user.groups.all():
                    if instance.groups.filter(pk=group.pk).exists():
                        return super().dispatch(request, *args, **kwargs)
            # No.
            else:
                return super().dispatch(request, *args, **kwargs)
        # No. Continue.
        else:
            return super().dispatch(request, *args, **kwargs)
        # This model has groups and the user is not part of them.
        messages.error(self.request, _("You don't have the required permissions."))
        return HttpResponseRedirect(reverse("home"))


class RedirectMixin:
    """
    Sets the self.next attribute if the next parameter in the url is valid

    Attributes:
        N/A
    """

    def dispatch(self, request, *args, **kwargs):
        # Get the next attribute from the URL.
        self.next = request.GET.get("next", None)
        # Make sure it is safe: not empty, not redirecting on another website, no spaces.
        if not self.next or "://" in self.next or " " in self.next:
            self.next = reverse("home")
        return super().dispatch(request, *args, **kwargs)
