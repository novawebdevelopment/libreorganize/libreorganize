import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from utils import force_login, setup_account, setup_browser
from selenium.webdriver.common.by import By

from apps.accounts.models import Account


class EventsTest(StaticLiveServerTestCase):
    fixtures = ["core/fixtures/initial_data.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()
        force_login(self.account, self.browser, self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def test_create_page(self):
        # Go to Home Directory
        self.browser.get(self.live_server_url + "/")

        # Go to Events Creation
        self.browser.get(self.live_server_url + "/events/create/")

        # Filling out the Form to create a Website
        self.browser.find_element(By.NAME, "title").send_keys("Test")
        self.browser.find_element(By.NAME, "description").send_keys("This is a Test")
        self.browser.find_element(By.NAME, "start_date").send_keys("2020-03-01 11:45")
        self.browser.find_element(By.NAME, "end_date").send_keys("2020-03-01 13:45")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element(By.NAME, "location").send_keys("1234 Washington Blvd")
        self.browser.find_element(By.XPATH, '//button').click()
        time.sleep(0.5)
        assert (
            "The event has been successfully created."
            in self.browser.find_element(By.CLASS_NAME, "alert-dismissible").text
        )

        # Go to Calender
        self.browser.get(self.live_server_url + "/events/?month=2020-3")

        # Check to See if Event was created
        assert "Test" in self.browser.page_source

        # Go to Edit Page and Edit Date
        self.browser.get(self.live_server_url + "/events/1/edit/")
        self.browser.find_element(By.NAME, "title").send_keys("Title")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element(By.XPATH, '//button').click()

        # Check April for Test
        self.browser.get(self.live_server_url + "/events/?month=2020-3")
        assert "Title" in self.browser.page_source

        # View test URL
        self.browser.get(self.live_server_url + "/events/1")
        assert "Title" in self.browser.page_source

        # Delete Event
        self.browser.get(self.live_server_url + "/events/1/delete/")
        self.browser.find_element(By.XPATH, '//button').click()
        time.sleep(0.5)
        assert "The event has been successfully deleted." in self.browser.find_element(By.CLASS_NAME, "alert").text

    def test_max_participants(self):
        # Go to Events Creation
        self.browser.get(self.live_server_url + "/events/create/")

        # Filling out the Form to create a Website
        self.browser.find_element(By.NAME, "title").send_keys("TestParticipants")
        self.browser.find_element(By.NAME,"description").send_keys("This is a Test")
        self.browser.find_element(By.NAME, "start_date").send_keys("2020-03-01 11:45")
        self.browser.find_element(By.NAME, "end_date").send_keys("2020-03-01 13:45")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element(By.NAME, "location").send_keys("1234 Washington Blvd")
        self.browser.find_element(By.XPATH, '//button').click()

        # Check alert that says event was created succesfully.
        time.sleep(0.5)
        assert "The event has been successfully created." in self.browser.find_element(By.CLASS_NAME, "alert").text

        # Test to make sure the event was created
        self.browser.get(self.live_server_url + "/events/2")
        assert "TestParticipants" in self.browser.page_source

        # Force Login
        self.client.force_login(Account.objects.create_user(email="test@test.com", password="test", first_name="Name"))
        self.browser.get(self.live_server_url + "/events/2/check/")
        # Check into Event
        time.sleep(0.5)
        assert (
            "You can only check in 24 hours before and after an event."
            in self.browser.find_element(By.CLASS_NAME, "alert").text
        )
