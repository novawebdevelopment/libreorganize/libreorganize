from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from utils import force_login, setup_account, setup_browser
from selenium.webdriver.common.by import By


class DashboardTest(StaticLiveServerTestCase):
    fixtures = ["core/fixtures/initial_data.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()
        force_login(self.account, self.browser, self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def test_dashboard(self):
        # Stefan wants to access some of the apps LibreOrganize offers.
        # He is already logged in, so he sees a dashboard button in the
        # upper right corner.
        self.browser.get(self.live_server_url + "/")
        self.browser.find_element(By.ID, "open-dashboard").click()

        # He sees a greeting message and a few buttons that link to
        # different apps.
        import time

        time.sleep(0.5)
        assert "Welcome to the Dashboard, Selenium-Test!" in self.browser.find_element(By.ID, "dashboard").text
