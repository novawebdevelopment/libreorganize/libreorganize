from django.test import TestCase

from apps.accounts.models import Account


class AccountsModelsTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json"]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_avatar_url(self):
        """Check avatar_url correct"""
        self.assertEqual(self.user.avatar_url, "/media/core/static/img/logos/libreorganize.png")
        self.assertEqual(self.superuser.avatar_url, "/static/img/avatar.png")

    def test_str(self):
        """Check __str__ correct"""
        self.assertEqual(self.user.__str__(), "Test User")
        self.assertEqual(self.superuser.__str__(), "Test Superuser")

    def test_full_name(self):
        """Check full_name correct"""
        self.assertEqual(self.user.full_name, "Test User")
        self.assertEqual(self.superuser.full_name, "Test Superuser")

    def test_is_superuser(self):
        """Check is_super_user correct"""
        self.assertEqual(self.user.is_superuser, False)
        self.assertEqual(self.superuser.is_superuser, True)
