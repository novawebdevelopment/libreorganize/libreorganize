from django.views import View
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from django.utils.html import strip_tags
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _

from core.mixins import ModelMixin, PermissionMixin, RedirectMixin
from core.views import CoreListView, CoreDetailView, CoreCreateView, CoreEditView, CoreDeleteView

from apps.accounts.forms import (
    AccountForm,
    AccountPermissionsForm,
    LoginForm,
    PasswordChangeForm,
    PasswordResetForm,
    RegisterForm,
    GroupForm,
)
from apps.accounts.models import Account, Group


class ListView(CoreListView):
    personal = False
    permission = "accounts.view_accounts"
    model = Account
    template_name = "accounts/list.html"


class DetailView(CoreDetailView):
    personal = True
    permission = "accounts.view_accounts"
    model = Account
    template_name = "accounts/detail.html"


class CreateView(CoreCreateView):
    personal = False
    permission = "accounts.manage_accounts"
    model = Account
    form_name = AccountForm
    template_name = "accounts/create.html"

    def post(self, request):
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save()
            messages.success(request, _("The account has been successfully created."))
            return HttpResponseRedirect(reverse("accounts:activate", args=[account.pk]) + f"?next={self.next}")
        return render(request=request, template_name=self.template_name, context={"form": form})


class EditView(CoreEditView):
    personal = True
    permission = "accounts.manage_accounts"
    model = Account
    form_name = AccountForm
    template_name = "accounts/edit.html"


class DeleteView(CoreDeleteView):
    personal = True
    permission = "accounts.manage_accounts"
    model = Account
    template_name = "accounts/delete.html"


class PasswordChangeView(PermissionMixin, ModelMixin, RedirectMixin, View):
    personal = True
    permission = "accounts.manage_accounts"
    model = Account

    def get(self, request):
        form = PasswordChangeForm()
        return render(request=request, template_name="accounts/password/change.html", context={"form": form})

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the password."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/password/change.html", context={"form": form})


class LoginView(RedirectMixin, View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            super().dispatch(request)
            messages.warning(request, _("You are already logged in!"))
            return HttpResponseRedirect(self.next)
        return super().dispatch(request)

    def get(self, request):
        form = LoginForm()
        return render(request=request, template_name="accounts/login.html", context={"form": form})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            form.login(request)
            messages.success(
                request,
                _("Welcome back, {first_name}! You have successfully logged in.").format(
                    first_name=request.user.first_name
                ),
            )
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/login.html", context={"form": form})


class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            messages.success(request, _("You have successfully logged out."))
        else:
            messages.warning(request, _("You are already logged out!"))
        return HttpResponseRedirect(reverse("home"))


class RegisterView(RedirectMixin, View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            super().dispatch(request)
            messages.warning(request, _("You cannot register an account while being logged in!"))
            return HttpResponseRedirect(self.next)
        return super().dispatch(request)

    def get(self, request):
        form = RegisterForm()
        return render(request=request, template_name="accounts/register.html", context={"form": form})

    def post(self, request):
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(
                request,
                _("Welcome to {website_name}, {first_name}! You have successfully registered.").format(
                    website_name=settings.WEBSITE_NAME, first_name=form.cleaned_data["first_name"]
                ),
            )
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/register.html", context={"form": form})


class PasswordResetView(View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            messages.warning(request, _("You have been redirected to change your password because you are logged in!"))
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.pk}))
        return super().dispatch(request)

    def get(self, request):
        form = PasswordResetForm()
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})

    def post(self, request):
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            account = form.save()
            url = request.build_absolute_uri("/accounts/password/reset/")
            url += urlsafe_base64_encode(force_bytes(account.pk)) + "/"
            url += default_token_generator.make_token(account) + "/"

            html = render_to_string(
                "email.html",
                {
                    "url": url,
                    "header": "Reset your password",
                    "message": "Use the link below to reset your password. It will expire after 24 hours.",
                    "button": "Reset Password",
                },
            )
            text = strip_tags(html).replace(str(_("Reset Password")), url)
            subject = f"Reset your password | {settings.WEBSITE_NAME}"

            if account.send_email(subject=subject, message=text, html_message=html):
                messages.success(request, _("You have successfully requested a password reset."))
                return HttpResponseRedirect(reverse("accounts:login"))
            else:
                messages.error(request, _("The password reset email could not be sent!"))
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})


class PasswordResetConfirmView(RedirectMixin, View):
    def dispatch(self, request, uidb64, token):
        if request.user.is_authenticated:
            messages.warning(request, _("You have been redirected to change your password because you are logged in!"))
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.pk}))
        try:
            self.account = Account.objects.get(pk=force_str(urlsafe_base64_decode(uidb64)))
            if default_token_generator.check_token(self.account, token):
                return super().dispatch(request)
            raise Account.DoesNotExist
        except (TypeError, ValueError, OverflowError, Account.DoesNotExist):
            messages.error(request, _("The password reset request is not valid."))
            return HttpResponseRedirect(reverse("accounts:password_reset"))

    def get(self, request):
        form = PasswordChangeForm()
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(account=self.account)
            messages.success(request, _("You have successfully reset your password."))
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})


class PermissionsChangeView(PermissionMixin, ModelMixin, RedirectMixin, View):
    personal = False
    permission = "superuser"
    model = Account

    def get(self, request):
        form = AccountPermissionsForm(instance=self.account)
        return render(request=request, template_name="accounts/permissions/change.html", context={"form": form})

    def post(self, request):
        form = AccountPermissionsForm(request.POST, instance=self.account)
        if form.is_valid():
            form.save(self.account)
            messages.success(request, _("You have successfully changed the account permissions."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/permissions/change.html", context={"form": form})


class ActivateView(PermissionMixin, ModelMixin, RedirectMixin, View):
    personal = False
    permission = "accounts.manage_accounts"
    model = Account

    def get(self, request):
        if self.account.is_active:
            messages.warning(request, _("The account has been already activated!"))
            return HttpResponseRedirect(reverse("home"))

        url = request.build_absolute_uri("/accounts/activate/")
        url += urlsafe_base64_encode(force_bytes(self.account.pk)) + "/"
        url += default_token_generator.make_token(self.account) + "/"

        html = render_to_string(
            "email.html",
            {
                "header": "Activate your account",
                "message": "Use the link below to activate your account. It will expire after 24 hours.",
                "url": url,
                "button": "Activate Account",
            },
        )
        text = strip_tags(html).replace(str(_("Activate Account")), url)
        subject = f"Activate your account | {settings.WEBSITE_NAME}"

        if self.account.send_email(subject=subject, message=text, html_message=html):
            messages.success(request, _("The activation email has been successfully sent."))
        else:
            messages.error(request, _("The activation email could not be sent!"))
        return HttpResponseRedirect(self.next)


class ActivateConfirmView(RedirectMixin, View):
    def dispatch(self, request, uidb64, token):
        if request.user.is_authenticated:
            messages.warning(request, _("You cannot activate the account while being logged in!"))
            return HttpResponseRedirect(reverse("accounts:detail", args={request.user.pk}))
        try:
            self.account = Account.objects.get(pk=force_str(urlsafe_base64_decode(uidb64)))
            if self.account.is_active:
                messages.warning(request, _("The account has been already activated!"))
                return HttpResponseRedirect(reverse("home"))
            if default_token_generator.check_token(self.account, token):
                return super().dispatch(request)
            raise Account.DoesNotExist
        except (TypeError, ValueError, OverflowError, Account.DoesNotExist):
            messages.error(request, _("The activation request is not valid!"))
            return HttpResponseRedirect(reverse("home"))

    def get(self, request):
        form = PasswordChangeForm()
        return render(
            request=request,
            template_name="accounts/activate.html",
            context={"form": form, "account": self.account},
        )

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            self.account.is_active = True
            form.save(self.account)
            messages.success(request, _("You have successfully activated your account."))
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(
            request=request,
            template_name="accounts/activate.html",
            context={"form": form, "account": self.account},
        )


class GroupListView(CoreListView):
    personal = False
    permission = "accounts.view_groups"
    model = Group
    template_name = "accounts/groups/list.html"


class GroupDetailView(CoreDetailView):
    personal = False
    permission = "accounts.view_groups"
    model = Group
    template_name = "accounts/groups/detail.html"


class GroupCreateView(CoreCreateView):
    personal = False
    permission = "accounts.manage_groups"
    model = Group
    form_name = GroupForm
    template_name = "accounts/groups/create.html"


class GroupEditView(CoreEditView):
    personal = False
    permission = "accounts.manage_groups"
    model = Group
    form_name = GroupForm
    template_name = "accounts/groups/edit.html"


class GroupDeleteView(CoreDeleteView):
    personal = False
    permission = "accounts.manage_groups"
    model = Group
    template_name = "accounts/groups/delete.html"
