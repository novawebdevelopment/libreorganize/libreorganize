from django.conf import settings
from django.urls import path

from apps.accounts import views

app_name = "apps.accounts"

urlpatterns = [
    # Require VIEW permissions
    path("", views.ListView.as_view(), name="list"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("groups/", views.GroupListView.as_view(), name="groups_list"),
    path("groups/<int:pk>/", views.GroupDetailView.as_view(), name="groups_detail"),
    # Require MANAGE permissions
    path("create/", views.CreateView.as_view(), name="create"),
    path("<int:pk>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<int:pk>/password/change/", views.PasswordChangeView.as_view(), name="password_change"),
    path("<int:pk>/permissions/change/", views.PermissionsChangeView.as_view(), name="permissions_change"),
    path("<int:pk>/activate/", views.ActivateView.as_view(), name="activate"),
    path("activate/<uidb64>/<token>/", views.ActivateConfirmView.as_view(), name="activate"),
    path("groups/create/", views.GroupCreateView.as_view(), name="groups_create"),
    path("groups/<int:pk>/edit/", views.GroupEditView.as_view(), name="groups_edit"),
    path("groups/<int:pk>/delete/", views.GroupDeleteView.as_view(), name="groups_delete"),
    # Require NO permissions
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("password/reset/", views.PasswordResetView.as_view(), name="password_reset"),
    path("password/reset/<uidb64>/<token>/", views.PasswordResetConfirmView.as_view(), name="password_reset"),
]

if settings.CAN_REGISTER:
    urlpatterns += [path("register/", views.RegisterView.as_view(), name="register")]
