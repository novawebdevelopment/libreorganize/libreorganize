import datetime
import re

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _
from tempus_dominus.widgets import DatePicker

from apps.accounts.models import Account, Group

TITLE_CHOICES = (
    ("", ""),
    ("mr", _("Mr.")),
    ("mx", _("Mx.")),
    ("mrs", _("Mrs.")),
    ("ms", _("Ms.")),
    ("miss", _("Miss")),
    ("dr", _("Dr.")),
    ("prof", _("Prof.")),
)

GENDER_CHOICES = (
    ("", ""),
    ("male", _("Male")),
    ("female", _("Female")),
    ("other", _("Other")),
)


class AccountForm(forms.ModelForm):
    verify_email = forms.EmailField(label="Verify Email")
    title = forms.ChoiceField(choices=TITLE_CHOICES, required=False)
    gender = forms.ChoiceField(choices=GENDER_CHOICES, required=False)

    class Meta:
        model = Account
        widgets = {
            "date_of_birth": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar fa-fw",
                        "up": "fas fa-arrow-up fa-fw",
                        "down": "fas fa-arrow-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            )
        }
        fields = (
            "email",
            "verify_email",
            "title",
            "first_name",
            "last_name",
            "date_of_birth",
            "gender",
            "company",
            "phone_number",
            "address_line1",
            "address_line2",
            "city",
            "state",
            "zipcode",
            "country",
            "avatar",
        )
        labels = {
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "date_of_birth": _("Date of Birth"),
            "company": _("Company / School / University"),
            "phone_number": _("Phone Number"),
            "address_line1": _("Address Line 1"),
            "address_line2": _("Address Line 2"),
            "state": _("State / Region / Province"),
            "zipcode": _("ZIP / Postal Code"),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, "instance", None)
        if instance and instance.pk:
            self.fields["verify_email"].initial = self.instance.email

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_verify_email(self):
        return self.cleaned_data["verify_email"].lower()

    def clean_first_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["first_name"]):
            raise forms.ValidationError(_("Enter a valid first name."))
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["last_name"]):
            raise forms.ValidationError(_("Enter a valid last name."))
        return self.cleaned_data["last_name"]

    def clean_date_of_birth(self):
        if self.cleaned_data["date_of_birth"] and (datetime.date.today() - self.cleaned_data["date_of_birth"]).days < 0:
            raise forms.ValidationError(_("The date of birth cannot be in the future."))
        return self.cleaned_data["date_of_birth"]

    def clean(self):
        self.cleaned_data = super().clean()
        if self.cleaned_data.get("email") and self.cleaned_data.get("verify_email"):
            if self.cleaned_data["email"] != self.cleaned_data["verify_email"]:
                raise forms.ValidationError(_("The emails do not match."))
        return self.cleaned_data

    def save(self):
        account = super().save(commit=False)
        if account._state.adding:
            account.is_active = False
            account.is_superuser = False
        account.save()
        return account


class LoginForm(forms.Form):
    email = forms.EmailField(label=_("Email"))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(render_value=False))

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean(self):
        if self.cleaned_data.get("email") and self.cleaned_data.get("password"):
            try:
                account = Account.objects.get(email=self.cleaned_data["email"])
                if not account.is_active:
                    raise forms.ValidationError(_("The account has not been activated."))
                self.account = authenticate(email=self.cleaned_data["email"], password=self.cleaned_data["password"])
                if self.account is None:
                    raise Account.DoesNotExist
            except Account.DoesNotExist:
                raise forms.ValidationError(_("The email and/or password you entered are incorrect."))
        return self.cleaned_data

    def login(self, request):
        login(request, self.account)


class RegisterForm(AccountForm):
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(render_value=False),
        help_text=_("Must have at least 8 characters, a letter, and a number"),
    )
    verify_password = forms.CharField(label=_("Verify Password"), widget=forms.PasswordInput(render_value=False))
    terms_of_service = forms.BooleanField(
        label=_("I agree to the Terms of Service"),
        error_messages={"required": "You must agree to the Terms of Service before registering."},
    )

    class Meta:
        model = Account
        widgets = {
            "date_of_birth": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar fa-fw",
                        "up": "fas fa-arrow-up fa-fw",
                        "down": "fas fa-arrow-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            )
        }
        fields = (
            "email",
            "verify_email",
            "password",
            "verify_password",
            "title",
            "first_name",
            "last_name",
            "date_of_birth",
            "gender",
            "company",
            "phone_number",
            "address_line1",
            "address_line2",
            "city",
            "state",
            "zipcode",
            "country",
            "avatar",
            "terms_of_service",
        )
        labels = {
            "verify_password": _("Verify Password"),
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "date_of_birth": _("Date of Birth"),
            "company": _("Company / School / University"),
            "phone_number": _("Phone Number"),
            "address_line1": _("Address Line 1"),
            "address_line2": _("Address Line 2"),
            "state": _("State / Region / Province"),
            "zipcode": _("ZIP / Postal Code"),
        }

    def clean_password(self):
        if not re.match(settings.PASSWORD_REGEX, self.cleaned_data["password"]):
            raise forms.ValidationError(_("The password needs to have at least 8 characters, a letter, and a number."))
        return self.cleaned_data["password"]

    def clean(self):
        self.cleaned_data = super().clean()
        if self.cleaned_data.get("password") and self.cleaned_data.get("verify_password"):
            if self.cleaned_data["password"] != self.cleaned_data["verify_password"]:
                raise forms.ValidationError(_("The passwords do not match."))
        return self.cleaned_data

    def save(self):
        account = super().save()
        account.is_active = True
        account.set_password(self.cleaned_data["password"])
        account.save()
        return account


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"))

    def clean_email(self):
        try:
            self.account = Account.objects.get(email=self.cleaned_data["email"].lower(), is_active=True)
        except Account.DoesNotExist:
            raise forms.ValidationError(_("The email is not associated with any active accounts."))
        return self.cleaned_data["email"].lower()

    def save(self):
        return self.account


class PasswordChangeForm(forms.Form):
    new_password = forms.CharField(label=_("New Password"), widget=forms.PasswordInput(render_value=False))
    verify_new_password = forms.CharField(
        label=_("Verify New Password"), widget=forms.PasswordInput(render_value=False)
    )

    def clean_new_password(self):
        if not re.match(settings.PASSWORD_REGEX, self.cleaned_data["new_password"]):
            raise forms.ValidationError(_("The password needs to have at least 8 characters, a letter, and a number."))
        return self.cleaned_data["new_password"]

    def clean(self):
        if self.cleaned_data.get("new_password") and self.cleaned_data.get("verify_new_password"):
            if self.cleaned_data["new_password"] != self.cleaned_data["verify_new_password"]:
                raise forms.ValidationError(_("The passwords do not match."))
        return self.cleaned_data

    def save(self, account):
        account.set_password(self.cleaned_data["new_password"])
        account.save()
        return account


class AccountPermissionsForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ("is_superuser", "user_permissions", "groups")
        labels = {"is_superuser": "Superuser", "user_permissions": "Permissions"}
        help_texts = {"user_permissions": ""}
        widgets = {"user_permissions": forms.CheckboxSelectMultiple, "groups": forms.CheckboxSelectMultiple}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["user_permissions"].choices = [
            (permission.pk, permission.content_type.app_label.capitalize() + ": " + permission.name)
            for permission in Permission.objects.exclude(
                content_type__app_label__in=["auth", "sessions", "contenttypes"]
            )
        ]


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ("name",)
