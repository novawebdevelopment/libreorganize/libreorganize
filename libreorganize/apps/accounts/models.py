from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import Group, PermissionsMixin
from django_countries.fields import CountryField
from django.core.mail import send_mail
from smtplib import SMTPException


from apps.accounts.managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    # Contains passoword and last_login (from AbstractBaseUser)
    # Contains is_superuser, user_permissions, and groups (from PermissionsMixin)
    email = models.EmailField(unique=True, error_messages={"unique": _("The email is already in use.")})
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)

    title = models.CharField(max_length=8, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=8, blank=True)
    phone_number = models.CharField(max_length=16, blank=True)
    company = models.CharField(max_length=64, blank=True)
    address_line1 = models.CharField(max_length=96, blank=True, help_text=_("Street address, P.O. box"))
    address_line2 = models.CharField(max_length=64, blank=True, help_text=_("Apartment, suite, unit, building, floor"))
    city = models.CharField(max_length=32, blank=True)
    state = models.CharField(max_length=32, blank=True)
    zipcode = models.CharField(max_length=16, blank=True)
    country = CountryField(blank_label="", blank=True)

    avatar = models.ImageField(upload_to="avatars", blank=True, help_text=_("Use a 1:1 image for the best quality"))

    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False, help_text=_("Grants all permissions"))
    groups = models.ManyToManyField(Group, related_name="members", blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    objects = AccountManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        ordering = ("last_name", "first_name")
        default_permissions = ()
        permissions = (
            ("view_accounts", "View accounts"),
            ("manage_accounts", "Manage accounts"),
        )

    @property
    def full_name(self):
        return self.first_name + " " + self.last_name

    @property
    def avatar_url(self):
        if self.avatar:
            return self.avatar.url
        return settings.STATIC_URL + "img/avatar.png"

    def __str__(self):
        return self.full_name

    def send_email(self, subject, message, html_message):
        try:
            send_mail(
                subject=subject,
                message=message,
                html_message=html_message,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=(self.email,),
            )
            return True
        except (ConnectionError, SMTPException):
            return False


class Group(Group):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        default_permissions = ()
        permissions = (
            ("view_groups", "View groups"),
            ("manage_groups", "Manage groups"),
        )
