from django.shortcuts import render
from django.views import View

from apps.boxes.forms import BoxForm, FileForm
from apps.boxes.models import Box, File
from core.views import CoreCreateView, CoreDeleteView, CoreEditView, CoreListView


class ListView(CoreListView):
    personal = False
    permission = "boxes.view_boxes"
    model = Box
    template_name = "boxes/list.html"


class EditView(CoreEditView):
    personal = False
    permission = "boxes.manage_boxes"
    model = Box
    form_name = BoxForm
    template_name = "boxes/edit.html"


class FileListView(CoreListView):
    personal = False
    permission = "boxes.view_files"
    model = File
    template_name = "boxes/files/list.html"


class FileUploadView(CoreCreateView):
    personal = False
    permission = "boxes.manage_files"
    model = File
    form_name = FileForm
    template_name = "boxes/files/create.html"


class FileDeleteView(CoreDeleteView):
    personal = False
    permission = "boxes.manage_files"
    model = File
    template_name = "boxes/files/delete.html"
