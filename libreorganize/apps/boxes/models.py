from django.db import models


class Box(models.Model):
    content = models.TextField()
    es_content = models.TextField(default="")
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("pk",)
        default_permissions = ()
        permissions = (
            ("view_boxes", "View boxes"),
            ("manage_boxes", "Edit boxes"),
        )


class File(models.Model):
    path = models.FileField(upload_to="files/")
    uploaded_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-uploaded_on",)
        default_permissions = ()
        permissions = (
            ("view_files", "View files"),
            ("manage_files", "Manage files"),
        )
