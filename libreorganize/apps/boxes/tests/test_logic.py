from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from apps.accounts.models import Account
from apps.boxes.models import Box


class BoxesListTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "core/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_successful(self):
        """Check list successful"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/", follow=True)
        self.assertContains(response, self.box.pk)
        self.assertContains(response, self.box.content)

    def test_incorrect_permissions(self):
        """Check list unsuccessful (permissions)"""
        self.client.force_login(self.user)
        response = self.client.get("/boxes/", follow=True)
        self.assertContains(response, "You don&#x27;t have the required permissions.")


class BoxesEditTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "core/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_successful(self):
        """Check edit successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            f"/boxes/{self.box.pk}/edit/", {"content": "Changed Content", "es_content": "Spanish Content"}, follow=True
        )
        self.assertContains(response, "The box has been successfully edited.")
        response = self.client.post(
            "/boxes/1/edit/", {"content": "Changed Content", "es_content": "Spanish Content"}, follow=True
        )
        response = self.client.get(f"/boxes/{self.box.pk}/edit/")
        self.assertContains(response, "Spanish Content")

    def test_incorrect_content(self):
        """Check edit unsuccessful (content)"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/boxes/{self.box.pk}/edit/", {"content": ""}, follow=True)
        self.assertContains(response, "This field is required.")

    def test_incorrect_box(self):
        """Check edit unsuccessful (box)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/999999/edit/", follow=True)
        self.assertContains(response, "The box doesn&#x27;t exist.")

    def test_incorrect_permissions(self):
        """Check edit unsuccessful (permissions)"""
        self.client.force_login(self.user)
        response = self.client.get(f"/boxes/{self.box.pk}/edit/", follow=True)
        self.assertContains(response, "You don&#x27;t have the required permissions.")


class BoxesUploadTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "core/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_successful(self):
        """Check successful get"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/files/?next=/boxes/", follow=True)
        self.assertContains(response, "Files")

    def test_unsuccessful_post(self):
        """check unsuccessful post"""
        self.client.force_login(self.superuser)
        response = self.client.post("/boxes/files/upload/", {"path": "uploaded"}, follow=True)
        self.assertContains(response, "This field is required.")


class BoxesUploadDeleteTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "core/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_successful(self):
        """Check successful get"""
        self.client.force_login(self.superuser)
        smalls_gif = (
            b"\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04"
            b"\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02"
            b"\x02\x4c\x01\x00\x3b"
        )
        uploaded = SimpleUploadedFile("smalls.gif", smalls_gif, content_type="image/gif")
        response = self.client.post("/boxes/files/upload/", {"path": uploaded}, follow=True)
        self.assertContains(response, "The file has been successfully created.")
        response = self.client.post("/boxes/files/1/delete/", follow=True)
        self.assertContains(response, "The file has been successfully deleted.")
