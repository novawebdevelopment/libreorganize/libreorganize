from django import forms
from django.utils.translation import gettext_lazy as _
from ckeditor.fields import CKEditorWidget

from apps.boxes.models import Box, File


class BoxForm(forms.ModelForm):
    class Meta:
        model = Box
        fields = "__all__"
        widgets = {"content": CKEditorWidget(), "es_content": CKEditorWidget()}
        labels = {"content": _("English Content"), "es_content": _("Spanish Content")}


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = "__all__"
        labels = {"path": "File"}
