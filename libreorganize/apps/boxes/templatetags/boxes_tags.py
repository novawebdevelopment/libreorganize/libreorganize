from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import get_language_from_request

from apps.boxes.models import Box

register = template.Library()


@register.simple_tag(takes_context=True)
def box(context, pk):
    request = context["request"]
    box = Box.objects.get_or_create(pk=pk)[0]
    if get_language_from_request(request) == "es":
        box = box.es_content
    else:
        box = box.content
    if request.user.has_perm("boxes.manage_boxes"):
        link = reverse("boxes:edit", args={pk}) + "?next=" + request.get_full_path()
        box += f'<a class="btn btn-link text-warning" href="{link}" data-toggle="tooltip" title="Edit Box"><i class="fas fa-edit fa-fw mr-1"></i></a>'
    return mark_safe(box)
