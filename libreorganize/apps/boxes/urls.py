from django.urls import path

from apps.boxes import views

app_name = "apps.boxes"

urlpatterns = [
    # Require VIEW permissions
    path("", views.ListView.as_view(), name="list"),
    path("files/", views.FileListView.as_view(), name="files_list"),
    # Require MANAGE permissions
    path("<int:pk>/edit/", views.EditView.as_view(), name="edit"),
    path("files/upload/", views.FileUploadView.as_view(), name="files_upload"),
    path("files/<int:pk>/delete/", views.FileDeleteView.as_view(), name="files_delete"),
]
