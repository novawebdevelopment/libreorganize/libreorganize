from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import Account
from apps.events.models import Event


class Command(BaseCommand):
    help = "Sends email notifcations whenever there is an event"

    def handle(self, *args, **options):
        startdate = datetime.now()
        enddate = startdate + relativedelta(hours=1)
        for event in Event.objects.filter(start_date__range=[startdate, enddate]):
            if not event.sent:
                for account in Account.objects.all():
                    # TODO: Replcae True with account settings check
                    if False:
                        url = f"https://test.com/events/{event.pk}/"
                        html = render_to_string(
                            "email.html",
                            {
                                "url": url,
                                "message": _(
                                    f"{account.first_name}, there is an upcoming event you might be interested in."
                                ),
                                "button": _("View Event"),
                            },
                        )
                        text = strip_tags(html).replace(_("View Event"), url)

                        send_mail(
                            subject=f"{_('Upcoming Event')} | {settings.WEBSITE_NAME}",
                            message=text,
                            html_message=html,
                            from_email=settings.EMAIL_HOST_USER,
                            recipient_list=(account.email,),
                            fail_silently=True,
                        )
                        self.stdout.write(self.style.SUCCESS(f"Event {event.pk} notification sent!"))
                        event.sent = True
                        event.save()
