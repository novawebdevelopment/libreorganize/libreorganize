import copy
from datetime import datetime, timedelta
from itertools import chain

from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import View

from apps.events.forms import CreateEventForm, EventForm
from apps.events.models import Event
from apps.events.utils import Calendar, get_date, next_month, prev_month
from core.mixins import ModelMixin, PermissionMixin
from core.views import CoreCreateView, CoreDeleteView, CoreDetailView, CoreEditView, CoreListView


class ListView(CoreListView):
    personal = False
    permission = "events.view_events"
    model = Event
    template_name = "events/list.html"

    def get(self, request):
        previous_btn = {"active": "", "pressed": "", "href": "?status=1", "text": "Show"}
        event_list = Event.objects.all()
        event_list = event_list.filter(start_date__gte=datetime.now())

        if self.request.GET.get("status", None) == "0":
            event_list = event_list.filter(start_date__gte=datetime.now())
            previous_btn = {"active": "active", "pressed": "", "href": "?status=1", "text": "Show"}
        elif self.request.GET.get("status", None) == "1":
            event_list = Event.objects.all()
            previous_btn = {"active": "", "pressed": "true", "href": "?status=0", "text": "Hide"}

        return render(
            request=request,
            template_name=self.template_name,
            context={"event_list": event_list, "previous_btn": previous_btn},
        )


class DetailView(CoreDetailView):
    personal = False
    permission = False
    model = Event
    template_name = "events/detail.html"

    def get(self, request):
        if not self.request.user.is_authenticated and self.event.is_private:
            return HttpResponseRedirect(reverse("accounts:login") + "?next=" + request.get_full_path())
        return super().get(request)


class CreateView(CoreCreateView):
    personal = False
    permission = "events.manage_events"
    model = Event
    form_name = CreateEventForm
    template_name = "events/create.html"

    def get_context(self):
        context = super().get_context()
        if self.request.GET.get("start_date", None):
            context["form"].initial["start_date"] = datetime.fromtimestamp(int(self.request.GET["start_date"]))
        return context

    def post(self, request):
        form = CreateEventForm(request.POST)
        if form.is_valid():
            # Save the form results
            self.event = form.save()
            messages.success(request, _("The event has been successfully created."))

            if self.event.occurrences > 1:
                delta = relativedelta(weeks=1)
                if self.event.frequency == "monthly":
                    delta = relativedelta(months=1)
                elif self.event.frequency == "yearly":
                    delta = relativedelta(years=1)

                for occurrence in range(self.event.occurrences - 1):
                    new_event = copy.copy(self.event)
                    new_event.pk = None
                    new_event.previous = self.event
                    new_event.start_date = self.event.start_date + delta
                    new_event.end_date = self.event.end_date + delta
                    new_event.save(force_insert=True)
                    self.event = new_event
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/edit.html", context={"form": form})


class EditView(CoreEditView):
    personal = False
    permission = "events.manage_events"
    model = Event
    form_name = EventForm
    template_name = "events/edit.html"

    def get_context(self):
        context = super().get_context()
        context["event"] = self.event
        return context

    def post(self, request):
        form = EventForm(request.POST, instance=self.event)
        if form.is_valid():
            # Check if we are editing only that event
            if "modalRadio" in self.request.POST and self.request.POST["modalRadio"] == "1":
                event = form.save()

            # Check if we are editing multiple events
            if "modalRadio" in self.request.POST and (
                self.request.POST["modalRadio"] == "2" or self.request.POST["modalRadio"] == "3"
            ):
                start_delta = form.cleaned_data["start_date"] - form.initial["start_date"]
                end_delta = form.cleaned_data["end_date"] - form.initial["end_date"]
                event = self.event

                # If updating all objects, start with the original object; otherwise start with the current object
                if self.request.POST["modalRadio"] == "3":
                    while event.previous is not None:
                        event = event.previous

                # For every object, update the attributes and adjust the start & end times
                try:
                    # Save the many-to-many fields and generic relations for this form.
                    cleaned_data = form.cleaned_data
                    exclude = form._meta.exclude
                    fields = form._meta.fields
                    opts = form.instance._meta

                    # don't set start_date and end_date
                    if exclude is None:
                        exclude = ["start_date", "end_date"]

                    while True:
                        for f in fields:
                            if f not in exclude and not f == "participants":
                                setattr(event, f, cleaned_data[f])

                        for f in chain(opts.many_to_many):
                            # if f.name in exclude:
                            #     continue
                            if f.name in cleaned_data:
                                f.save_form_data(event, cleaned_data[f.name])

                        if event.pk != self.event.pk:
                            event.start_date = event.start_date + start_delta
                            event.end_date = event.end_date + end_delta

                        event.save()  # save the object
                        event = event.subsequent  # now repeat for next object in list
                except Event.DoesNotExist:
                    pass
            messages.success(request, _("The event has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/edit.html", context={"form": form})


class DeleteView(CoreDeleteView):
    personal = False
    permission = "events.manage_events"
    model = Event
    template_name = "events/delete.html"

    def get_context(self):
        context = super().get_context()
        context["event"] = self.event
        return context

    def post(self, request):
        if "modalRadio" in self.request.POST and self.request.POST["modalRadio"] == "1":
            event = self.event
            try:
                subsequent = event.subsequent
                subsequent.previous = event.previous
                event.previous = None
                event.save()
                subsequent.save()
            except Event.DoesNotExist:
                pass
        self.event.delete()
        messages.success(request, _("The event has been successfully deleted."))
        return HttpResponseRedirect(self.next)


class CalendarView(View):
    def get(self, request, *args, **kwargs):
        date = get_date(self.request.GET.get("month", None))
        calendar = Calendar(date.year, date.month)

        context = {
            "calendar": calendar.formatmonth(withyear=True, privateevents=request.user.is_authenticated),
            "prev_month": prev_month(date),
            "next_month": next_month(date),
            "date": date,
        }

        return render(request=request, template_name="events/calendar.html", context=context)


class CheckView(PermissionMixin, ModelMixin, View):
    personal = False
    permission = True
    model = Event

    def get(self, request):
        # Redirect if checking in is not enabled for this event
        if not self.event.can_check_in:
            messages.add_message(request, messages.ERROR, _("You cannot check in to this event."))
            return HttpResponseRedirect(reverse("events:detail", args={self.event.pk}))

        event_start = self.event.start_date - timedelta(days=1)
        event_end = self.event.end_date + timedelta(days=1)
        if request.user in self.event.participants.all():
            messages.add_message(request, messages.WARNING, _("You have already checked in!"))
        else:
            if self.event.participants.count() >= self.event.max_participants and self.event.max_participants != -1:
                messages.add_message(request, messages.ERROR, _("There are no more empty seats for this event!"))
            elif event_start < datetime.now() < event_end:
                self.event.participants.add(request.user)
                messages.add_message(request, messages.SUCCESS, _("You checked in."))
            else:
                messages.add_message(
                    request, messages.ERROR, _("You can only check in 24 hours before and after an event.")
                )
        return HttpResponseRedirect(reverse("events:detail", args={self.event.pk}))
