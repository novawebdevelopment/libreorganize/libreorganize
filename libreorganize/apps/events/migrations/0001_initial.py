# Generated by Django 2.2.13 on 2020-11-11 23:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=48)),
                ('description', models.TextField(blank=True)),
                ('start_date', models.DateTimeField(help_text='Must be formatted as YYYY-MM-DD HH:MM')),
                ('end_date', models.DateTimeField(help_text='Must be formatted as YYYY-MM-DD HH:MM')),
                ('location', models.CharField(help_text='Should be an address', max_length=128)),
                ('max_participants', models.IntegerField(default=-1, help_text='Enter -1 for an unlimited number of participants')),
                ('is_private', models.BooleanField(default=False, help_text='Must be logged in to view the event')),
                ('occurrences', models.IntegerField(default=1, help_text='Must be a value between 1 and 12')),
                ('frequency', models.CharField(choices=[('weekly', 'Weekly'), ('monthly', 'Monthly'), ('yearly', 'Yearly')], default='weekly', help_text='Ignore if there is only one occurrence', max_length=12)),
                ('sent', models.BooleanField(default=False)),
                ('can_check_in', models.BooleanField(default=True, help_text='Allow atendees to check in to this event')),
                ('participants', models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL)),
                ('previous', models.OneToOneField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='subsequent', to='events.Event')),
            ],
            options={
                'permissions': (('view_events', 'View events'), ('manage_events', 'Manage events')),
                'default_permissions': (),
            },
        ),
    ]
