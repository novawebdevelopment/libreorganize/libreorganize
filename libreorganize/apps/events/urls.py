from django.urls import path

from apps.events import utils, views

app_name = "apps.events"

urlpatterns = [
    # Require VIEW permissions
    path("", views.ListView.as_view(), name="list"),
    # Require MANAGE permissions
    path("create/", views.CreateView.as_view(), name="create"),
    path("<int:pk>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<int:pk>/check/", views.CheckView.as_view(), name="check"),
    # Require NO permissions
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("calendar/", views.CalendarView.as_view(), name="calendar"),
    path("subscribe/", utils.EventFeed(), name="subscribe"),
]
