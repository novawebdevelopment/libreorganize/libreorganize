from django.test import TestCase
from django.urls import resolve


class WikisUrlsCase(TestCase):
    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/votations/create/").view_name, "votations:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/votations/1/").view_name, "votations:detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/votations/1/edit/").view_name, "votations:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/votations/1/delete/").view_name, "votations:delete")

    def test_public_list(self):
        """Check public list URL correct"""
        self.assertEqual(resolve("/votations/list/").view_name, "votations:public_list")

    def test_normal(self):
        """Check normal vote URL correct"""
        self.assertEqual(resolve("/votations/1/vote/").view_name, "votations:vote")

    def test_ranked(self):
        """Check ranked vote URL correct"""
        self.assertEqual(resolve("/votations/1/voterank/").view_name, "votations:voterank")

    def test_results(self):
        """Check ranked vote URL correct"""
        self.assertEqual(resolve("/votations/1/results/").view_name, "votations:results")
