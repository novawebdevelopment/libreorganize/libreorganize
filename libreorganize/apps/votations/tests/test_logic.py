import datetime
import time

from django.test import TestCase

from apps.accounts.models import Account


class TestVotationsListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_closed_status(self):
        """Successful closed view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations")
        response = self.client.get("/votations/?status=1", follow=True)
        self.assertContains(response, "Closed")

    def test_pending_status(self):
        """Successful pending view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations")
        response = self.client.get("/votations/", follow=True)
        self.assertContains(response, "Pending")

    def test_active_status(self):
        """Successful active view"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations")
        response = self.client.get("/votations/", follow=True)
        self.assertContains(response, "Open")


class TestVotationsCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_get_view(self):
        """Successful get create view"""
        self.client.force_login(self.superuser)
        response = self.client.get(
            "/votations/create/",
            follow=True,
        )
        self.assertContains(response, "Create Votation")

    def test_normal_create_view(self):
        """Successful normal create view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations")

    def test_ranked_create_view(self):
        """Successful ranked create View"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations")

    def test_invalid_perms(self):
        """Invalid perms"""
        self.client.force_login(self.user)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "You don&#x27;t have the required permissions.")

    def test_invalid_date(self):
        """Start date after end date"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "The start date must be less than the end date.")

    def test_one_choice(self):
        """Test one choice"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-TOTAL_FORMS": "1",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations must have at least two choices.")

    def test_unique_choice(self):
        """Test unique choices"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 1",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Votations must have unique choices.")

    def test_invalid_edit(self):
        """invalid edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Create Votation")


class TestVotationsEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_successful_edit(self):
        """Successful edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/edit/",
            {
                "topic": "Question Normal Edit",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "b",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "The votation has been successfully edited.")

    def test_poll_active(self):
        """Votations is active so no editing"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/edit/",
            follow=True,
        )
        self.assertContains(response, "You cannot edit a votation after it has started.")

    def test_get_edit(self):
        """Successful get edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/edit/",
            follow=True,
        )
        self.assertContains(response, "Edit Votation")
        response = self.client.get(
            "/votations/1/edit/",
            follow=True,
        )
        self.assertContains(response, "Edit Votation")

    def test_invalid_edit(self):
        """invalid edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/edit/",
            {
                "topic": "",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "b",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Edit Votation")


class TestVotationsDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_successful_delete(self):
        """Test successful delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/delete/",
            follow=True,
        )
        self.assertContains(response, "The votation has been successfully deleted.")


class TestVotationsPublicListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_public_list(self):
        """Test public list"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/list/",
            follow=True,
        )
        self.assertContains(response, "Question Normal")


class TestVotationsVoteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_template(self):
        """Check login template"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/votations/1/vote/", follow=True)
        self.assertTemplateUsed(response, template_name="votations/normalvote.html")

    def test_get_vote(self):
        """Test get method vote"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/vote/",
            follow=True,
        )
        self.assertContains(response, "Question Normal")

    def test_successful_vote(self):
        """Test get method vote"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/vote/",
            {"choice_pk": 1},
            follow=True,
        )
        self.assertRedirects(response, "/votations/list/")

    def test_already_voted(self):
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/vote/",
            {"choice_pk": 1},
            follow=True,
        )
        response = self.client.get(
            "/votations/1/vote/",
            follow=True,
        )
        self.assertContains(response, "You have already voted!")
        self.assertRedirects(response, "/votations/list/")

    def test_poll_closed(self):
        """Test votation closed"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/vote/",
            follow=True,
        )
        self.assertContains(response, "The votation is not open for voting.")

    def test_redirect_not_signedin(self):
        """redirect if user not signed in"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.client.logout()
        response = self.client.get(
            "/votations/1/vote/",
            follow=True,
        )
        self.assertRedirects(response, "/accounts/login/")


class TestVotationsVoteRankedView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_template(self):
        """Check login template"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/votations/1/vote/", follow=True)
        self.assertTemplateUsed(response, template_name="votations/rankvote.html")

    def test_get_vote(self):
        """Test get method vote"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/voterank/",
            follow=True,
        )
        self.assertContains(response, "Question Normal")

    def test_poll_closed(self):
        """Test votation closed"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/voterank/",
            follow=True,
        )
        self.assertContains(response, "The votation is not open for voting.")

    def test_redirect_not_signedin(self):
        """redirect if user not signed in"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.client.logout()
        response = self.client.get(
            "/votations/1/voterank/",
            follow=True,
        )
        self.assertRedirects(response, "/accounts/login/")

    def test_already_voted(self):
        """test already voted ranked"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/voterank/",
            {"result-0": 1, "result-1": 2},
            follow=True,
        )
        response = self.client.get(
            "/votations/1/voterank/",
            follow=True,
        )
        self.assertRedirects(response, "/votations/list/")
        self.assertContains(response, "You have already voted!")


class TestVotationsVoteResultsView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_results_normal(self):
        """Test results normal"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/vote/",
            {"choice_pk": 1},
            follow=True,
        )
        time.sleep(1)
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "choice 1")

    def test_results_ranked(self):
        """Test results ranked"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/voterank/",
            {"result-0": 1, "result-1": 2},
            follow=True,
        )
        time.sleep(1)
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "choice 1")

    def test_results_closed(self):
        """Make sure can't view results till votation closed"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "The votation is not yet closed.")
        self.assertRedirects(response, "/votations/list/")

    def test_results_ranked_tied(self):
        """Test results ranked tied"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "ranked",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/votations/1/voterank/",
            {"result-0": 1, "result-1": 2},
            follow=True,
        )
        self.client.force_login(self.user)
        response = self.client.post(
            "/votations/1/voterank/",
            {"result-0": 2, "result-1": 1},
            follow=True,
        )
        time.sleep(1)
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "Results")
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "Results")

    def test_no_votes(self):
        """Test no votes"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/votations/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "votation_type": "normal",
                "start_date": start,
                "end_date": end,
                "votationchoice_set-0-pk": "",
                "votationchoice_set-0-name": "choice 1",
                "votationchoice_set-1-pk": "",
                "votationchoice_set-1-name": "choice 2",
                "votationchoice_set-TOTAL_FORMS": "3",
                "votationchoice_set-INITIAL_FORMS": "0",
                "votationchoice_set-MIN_NUM_FORMS": "0",
                "votationchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        time.sleep(1)
        response = self.client.get(
            "/votations/1/results/",
            follow=True,
        )
        self.assertContains(response, "Results")
