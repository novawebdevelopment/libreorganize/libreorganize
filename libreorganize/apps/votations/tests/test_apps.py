from django.test import TestCase

from apps.votations.apps import VotationsConfig


class VotationsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(VotationsConfig.name, "apps.votations")
