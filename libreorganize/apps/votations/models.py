import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import Account, Group


class Votation(models.Model):
    # This represents a vote to be taken
    topic = models.CharField(max_length=128)
    description = models.TextField()
    votation_type = models.CharField(max_length=16, default="normal", choices=settings.TYPE_VOTING)
    start_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    end_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    groups = models.ManyToManyField(Group, related_name="votations", blank=True)
    winner = models.OneToOneField("VotationChoice", null=True, blank=True, on_delete=models.CASCADE, related_name="+")
    voters = models.ManyToManyField(Account, related_name="votations")

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_votations", "List votations"),
            ("manage_votations", "Manage votations"),
        )

    def get_votation_status(self):
        now = datetime.datetime.now()
        if now >= self.end_date:
            return "closed"
        elif self.start_date <= now < self.end_date:
            return "open"
        return "pending"

    def get_votation_status_display(self):
        now = datetime.datetime.now()
        if self.start_date <= now < self.end_date:
            return _("Open")
        if now >= self.end_date:
            return _("Closed")
        return _("Pending")


class VotationChoice(models.Model):
    # This represents an individual candidate or choice in a votation
    name = models.CharField(max_length=64)
    votation = models.ForeignKey(Votation, on_delete=models.CASCADE)

    class Meta:
        default_permissions = ()
        ordering = ("name",)


class VotationVote(models.Model):
    class Meta:
        default_permissions = ()


class VotationDecision(models.Model):
    # This represents a cast vote or elected choice made by an individual for VotationChoice (e.g., candidate) in a Votation
    vote = models.ForeignKey(VotationVote, on_delete=models.CASCADE, default="", null=True, blank=True)
    votation = models.ForeignKey(Votation, on_delete=models.CASCADE)
    choice = models.ForeignKey(VotationChoice, on_delete=models.CASCADE)
    time_cast = models.DateTimeField(auto_now_add=True)
    rank = models.IntegerField(null=True, blank=True)

    class Meta:
        default_permissions = ()
