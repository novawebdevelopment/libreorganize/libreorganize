from django.urls import path

from apps.votations import views

app_name = "apps.votations"

urlpatterns = [
    # Requires LIST permissions
    path("", views.ListView.as_view(), name="list"),
    # Requires NO permissions
    path("list/", views.PublicListView.as_view(), name="public_list"),
    path("<int:pk>/results/", views.ResultView.as_view(), name="results"),
    # Requires MANAGE permissions
    path("create/", views.CreateView.as_view(), name="create"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("<int:pk>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.DeleteView.as_view(), name="delete"),
    # Requires user to AUTHENTICATE
    path("<int:pk>/vote/", views.VoteView.as_view(), name="vote"),
    path("<int:pk>/voterank/", views.VoteRankedView.as_view(), name="voterank"),
]
