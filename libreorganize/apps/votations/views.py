from datetime import datetime
from random import randint

from django.contrib import messages
from django.db import transaction
from django.db.models import Count
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django.shortcuts import render
from django.core.paginator import Paginator

from apps.votations.forms import (
    VotationChoiceModelForm,
    VotationChoicesFormSet,
    VotationDecisionForm,
    VotationForm,
    VotationRankedDecisionForm,
)
from apps.votations.models import Votation, VotationChoice, VotationDecision, VotationVote
from apps.votations.utils import Election
from core.mixins import PermissionMixin, GroupPermissionMixin
from core.views import CoreCreateView, CoreDeleteView, CoreDetailView, CoreEditView, CoreListView


class ListView(CoreListView):
    personal = False
    permission = "votations.list_votations"
    model = Votation
    template_name = "votations/list.html"

    def get(self, request):
        previous_btn = {"active": "", "pressed": "", "href": "?status=1", "text": "Show"}
        votations = Votation.objects.all()
        votations = votations.filter(end_date__gte=datetime.now())

        if self.request.GET.get("status", None) == "0":
            votations = votations.filter(end_date__gte=datetime.now())
            previous_btn = {"active": "active", "pressed": "", "href": "?status=1", "text": "Show"}
        elif self.request.GET.get("status", None) == "1":
            votations = Votation.objects.all()
            previous_btn = {"active": "", "pressed": "true", "href": "?status=0", "text": "Hide"}

        return render(
            request=request,
            template_name=self.template_name,
            context={"votation_list": votations, "previous_btn": previous_btn},
        )


class DetailView(CoreDetailView):
    personal = False
    permission = "votations.manage_votations"
    model = Votation
    template_name = "votations/detail.html"


class DeleteView(CoreDeleteView):
    personal = False
    permission = "votations.manage_votations"
    model = Votation
    template_name = "votations/delete.html"


class CreateView(CoreCreateView):
    personal = False
    permission = "votations.manage_votations"
    model = Votation
    form_name = VotationForm
    template_name = "votations/create.html"
    choices_formset_factory = inlineformset_factory(
        parent_model=Votation,
        model=VotationChoice,
        form=VotationChoiceModelForm,
        formset=VotationChoicesFormSet,
        can_order=False,
        can_delete=False,
    )

    def get(self, request, *args, **kwargs):
        """Overriding get method to handle inline formset."""
        self.object = None
        form = VotationForm()
        formset = self.choices_formset_factory()
        return render(request=request, template_name=self.template_name, context={"form": form, "choices": formset})

    def post(self, request, *args, **kwargs):
        """Overriding post method to handle inline formsets."""
        self.object = None
        form = VotationForm(request.POST)
        formset = self.choices_formset_factory(self.request.POST)

        if form.is_valid():
            self.object = form.save(commit=False)
            formset = self.choices_formset_factory(self.request.POST, instance=self.object)
            if formset.is_valid():
                self.object.save()
                formset.save()
                return HttpResponseRedirect(self.next)

        return render(request=request, template_name=self.template_name, context={"form": form, "choices": formset})


class EditView(CoreEditView):
    personal = False
    permission = "votations.manage_votations"
    model = Votation
    form_name = VotationForm
    template_name = "votations/edit.html"
    choices_formset_factory = inlineformset_factory(
        parent_model=Votation,
        model=VotationChoice,
        form=VotationChoiceModelForm,
        formset=VotationChoicesFormSet,
        extra=1,
        can_order=False,
        can_delete=True,
    )

    def get(self, request, *args, **kwargs):
        """Overriding get method to handle inline formset."""
        if self.votation.get_votation_status() != "pending":
            messages.error(request, "You cannot edit a votation after it has started.")
            return HttpResponseRedirect(self.next)
        form = VotationForm(instance=self.votation)
        formset = self.choices_formset_factory(instance=self.votation)
        return render(request=request, template_name=self.template_name, context={"form": form, "choices": formset})

    def post(self, request, *args, **kwargs):
        """Overriding post method to handle inline formsets."""
        form = VotationForm(request.POST, instance=self.votation)
        formset = self.choices_formset_factory(self.request.POST, instance=self.votation)

        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            messages.success(request, _("The votation has been successfully edited."))
            return HttpResponseRedirect(self.next)
        else:
            return render(request=request, template_name=self.template_name, context={"form": form, "choices": formset})


class PublicListView(CoreListView):
    personal = False
    permission = False
    model = Votation
    paginate_by = 5
    template_name = "votations/publiclist.html"

    def get_context(self):
        context = super().get_context()
        context["date"] = datetime.now()
        context["num_of_elements"] = context["votation_list"].count()
        return context

    def get(self, request, *args, **kwargs):
        votations = Votation.objects.all().order_by("-end_date")
        paginator = Paginator(votations, 5)

        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        return render(request, self.template_name, {'page_obj': page_obj})

class VoteView(PermissionMixin, GroupPermissionMixin, FormView):
    personal = False
    permission = True
    form_class = VotationDecisionForm
    votation = None
    model = Votation
    decision = None
    success_url = "/votations/list/"
    template_name = "votations/normalvote.html"

    def load_votation(self, pk):
        self.votations = Votation.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.decision
        context["votation"] = self.votations
        return context

    def dispatch(self, request, *args, **kwargs):
        self.load_votation(kwargs["pk"])
        if self.votations.get_votation_status() != "open":
            messages.error(request, "The votation is not open for voting.")
            return HttpResponseRedirect(self.get_success_url())
        elif not self.request.user.is_authenticated:
            return HttpResponseRedirect("/accounts/login/")

        user = self.request.user
        if user.votations.filter(pk=self.votations.pk).exists():
            messages.error(request, "You have already voted!")
            return HttpResponseRedirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        with transaction.atomic():
            # Save our vote
            instance = VotationDecision()
            instance.votation = self.votations
            instance.choice = VotationChoice.objects.get(pk=form.cleaned_data["choice_pk"])
            instance.save()
            # Save that we voted
            self.request.user.votations.add(self.votations)
            self.request.user.save()

        return super().form_valid(form)

    def get_template_names(self):
        if self.votations.votation_type == "ranked":
            return ["votations/rankvote.html"]
        else:
            return ["votations/normalvote.html"]


class VoteRankedView(PermissionMixin, GroupPermissionMixin, FormView):
    personal = False
    permission = True
    form_class = VotationRankedDecisionForm
    success_url = "/votations/list/"
    votation = None
    model = Votation
    template_name = "votations/rankvote.html"

    def load_votation(self, pk):
        self.votation = Votation.objects.get(pk=pk)

    def dispatch(self, request, *args, **kwargs):
        self.load_votation(kwargs["pk"])
        if self.votation.get_votation_status() != "open":
            messages.error(request, "The votation is not open for voting.")
            return HttpResponseRedirect(self.get_success_url())
        elif not self.request.user.is_authenticated:
            return HttpResponseRedirect("/accounts/login/")

        user = self.request.user
        if user.votations.filter(pk=self.votation.pk).exists():
            messages.error(request, "You have already voted!")
            return HttpResponseRedirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            # Save that we voted
            self.request.user.votations.add(self.votation)
            self.request.user.save()

            # Create a vote
            vote = VotationVote()
            vote.save()

            # Save our candidate decisions
            for r in range(VotationChoice.objects.filter(votation=self.votation).count()):
                instance = VotationDecision()
                instance.votation = self.votation
                instance.choice = VotationChoice.objects.get(pk=request.POST.get("result-" + str(r)))
                instance.rank = r
                instance.vote = vote
                instance.save()

        return super().post(request, *args, **kwargs)


class ResultView(CoreDetailView):
    personal = False
    permission = False
    model = Votation
    template_name = "votations/results.html"

    def get(self, request, *args, **kwargs):
        if self.votation.get_votation_status() != "closed":
            messages.error(request, "The votation is not yet closed.")
            return HttpResponseRedirect("/votations/list/")
        return super().get(request, *args, **kwargs)

    def get_normal_results(self, votation):
        # Get a set of results [ { choice1, total1 }, { choice2, total2 }, ...]
        results = (
            VotationDecision.objects.filter(votation=votation)
            .values_list("choice__name")
            .annotate(total=Count("choice__name"))
            .order_by("-total")
        )

        # If the winner is not already saved, make sure to save it
        if not votation.winner:
            winners = []

            if len(results) > 0:
                # Always append the first vote to the winners
                winners.append(results[0][0])

                # If a tie exists, append all tied votes
                if len(results) > 1 and results[0][1] == results[1][1]:
                    for r in range(len(results) - 1):
                        if results[0][1] == results[r + 1][1]:
                            winners.append(results[r + 1][0])
            else:
                # If no one voted, pick among the decisions randomly
                for choice in VotationChoice.objects.filter(votation=votation):
                    winners.append(choice.name)

            # If we have a tie
            if len(winners) > 1:
                coinflip = randint(0, len(winners) - 1)
                winner_pk = winners[coinflip]
            else:
                winner_pk = winners[0]

            winner = VotationChoice.objects.get(votation=votation, name=winner_pk)
            votation.winner = winner
            votation.save()

        return results

    def get_ranked_results(self, votation):
        # Check if a stored winner exists.  If so, return that object
        if votation.winner:
            return votation.winner

        # Compute the winner
        decisions = VotationDecision.objects.filter(votation=votation).values_list("vote", "choice__pk").order_by("vote", "rank")
        choices = VotationChoice.objects.filter(votation=votation).values_list("pk")
        choicelist = [item for t in choices for item in t]
        election = Election(choicelist)

        vote = None
        voter_votes = None
        for decision in decisions:
            # When we changed voters - submit this vote and restart the vote collection
            if vote != decision[0]:
                if voter_votes:
                    election.vote(voter_votes)
                vote = decision[0]
                voter_votes = []

            # Append the next decision to my voter list
            voter_votes.append(decision[1])

        # Ensure the last vote counts!
        if voter_votes:
            election.vote(voter_votes)

        # Save this result and load it for next time!
        winner = election.tally()
        result = VotationChoice.objects.get(pk=winner)
        votation.winner = result
        votation.save()

        return result

    def get_context(self, **kwargs):
        context = super().get_context()
        context["votation"] = self.votation

        if self.votation.votation_type == "normal":
            context["results"] = self.get_normal_results(self.votation)
        else:
            context["result"] = self.get_ranked_results(self.votation)
        return context
