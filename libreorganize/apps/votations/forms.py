from django import forms
from django.forms import BaseInlineFormSet
from django.utils.translation import gettext_lazy as _
from tempus_dominus.widgets import DateTimePicker

from apps.votations.models import Votation, VotationChoice


class VotationForm(forms.ModelForm):
    class Meta:
        model = Votation
        fields = ["topic", "description", "votation_type", "start_date", "end_date", "groups"]
        labels = {
            "topic": "Topic",
            "description": "Description",
            "votation_type": "Type",
            "start_date": "Start Date",
            "end_date": "End Date",
        }
        widgets = {
            "start_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "end_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "groups": forms.CheckboxSelectMultiple,
        }

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["start_date"] > self.cleaned_data["end_date"]:
            raise forms.ValidationError(_("The start date must be less than the end date."))
        return self.cleaned_data


class VotationChoicesFormSet(BaseInlineFormSet):
    def clean(self):
        choices = []
        duplicates = False
        count = 0

        for form in self.forms:
            if form.cleaned_data:
                # On 'create', we don't have DELETE elements.  If we do have a 'DELETE' element, we skip it anyway
                if "DELETE" not in form.cleaned_data.keys() or not form.cleaned_data["DELETE"]:
                    choice = form.cleaned_data["name"]
                    count += 1
                    if choice in choices:
                        duplicates = True
                    else:
                        choices.append(choice)

        if count < 2:
            raise forms.ValidationError(_("Votations must have at least two choices."))

        if duplicates:
            raise forms.ValidationError(_("Votations must have unique choices."))


class VotationDecisionForm(forms.Form):
    choice_pk = forms.IntegerField(required=True)


class VotationChoiceModelForm(forms.ModelForm):
    class Meta:
        model = VotationChoice
        fields = ["name"]
        labels = {"name": "Answer"}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # For some reason, using this as the widget in Meta doesn't work, so applying it here
        self.fields["name"].widget.attrs["class"] = "textinput textInput form-control"
        self.fields["name"].widget.attrs["placeholder"] = "Enter Candidate/Choice/Answer Here"


class VotationRankedDecisionForm(forms.Form):
    pass
