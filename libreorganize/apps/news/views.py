from smtplib import SMTPException

from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMessage, send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views import View
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _

from apps.news.forms import AnnouncementForm, NewsletterForm, SendMailForm
from apps.news.models import Announcement, Newsletter
from core.views import CoreCreateView, CoreDeleteView, CoreDetailView, CoreEditView, CoreListView


class AnnouncementListView(CoreListView):
    personal = False
    permission = "announcements.view_announcements"
    model = Announcement
    template_name = "news/announcements/list.html"


class AnnouncementCreateView(CoreCreateView):
    personal = False
    permission = "announcements.manage_announcements"
    model = Announcement
    form_name = AnnouncementForm
    template_name = "news/announcements/create.html"

    def post(self, request):
        form = AnnouncementForm(request.POST)
        if form.is_valid():
            announcement = form.save()
            messages.success(request, _("The announcement has been successfully created."))
            announcement.author = self.request.user
            announcement.save()
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name=self.template_name, context={"form": form})


class AnnouncementDetailView(CoreDetailView):
    personal = False
    permission = False
    model = Announcement
    template_name = "news/announcements/detail.html"


class AnnouncementEditView(CoreEditView):
    personal = False
    permission = "announcements.manage_announcements"
    model = Announcement
    form_name = AnnouncementForm
    template_name = "news/announcements/edit.html"


class AnnouncementDeleteView(CoreDeleteView):
    personal = False
    permission = "announcements.manage_announcements"
    model = Announcement
    template_name = "news/announcements/delete.html"


class NewsletterListView(CoreListView):
    personal = False
    permission = "newsletters.view_newsletter"
    model = Newsletter
    template_name = "news/newsletters/list.html"


class NewsletterCreateView(CoreCreateView):
    personal = False
    permission = False
    model = Newsletter
    form_name = NewsletterForm
    template_name = "news/newsletters/create.html"

    def post(self, request, *args, **kwargs):
        form = NewsletterForm(request.POST)
        if form.is_valid():
            newsletter = form.save(commit=False)
            newsletter.token = get_random_string(length=32)
            newsletter.is_superuser = False
            newsletter.save()
            messages.success(request, _("The newsletter user has been successfully added."))

            html = render_to_string(
                "email.html",
                {
                    "message": f"{newsletter.full_name}, You have been subscribed to {settings.WEBSITE_NAME} Newsletters",
                },
            )
            send_mail(
                subject=f"Welcome to | {settings.WEBSITE_NAME}",
                message=html,
                html_message=html,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=(newsletter.email,),
                fail_silently=True,
            )
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="news/newsletters/create.html", context={"form": form})


class NewsletterEditView(CoreEditView):
    personal = False
    permission = "newsletters.manage_newsletter"
    model = Newsletter
    form_name = NewsletterForm
    template_name = "news/newsletters/edit.html"


class NewsletterDeleteView(CoreDeleteView):
    personal = False
    permission = "newsletters.manage_newsletter"
    model = Newsletter
    template_name = "news/newsletters/delete.html"


class NewsletterSendMailView(CoreCreateView):
    personal = False
    permission = "newsletters.manage_newsletter"
    form_name = SendMailForm
    template_name = "news/newsletters/sendmail.html"

    def post(self, request):
        domainname = request.META["HTTP_HOST"]

        form = SendMailForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data["languages"] == "all":
                emails = [n.email for n in Newsletter.objects.all()]
            else:
                emails = [n.email for n in Newsletter.objects.filter(language=form.cleaned_data["languages"])]

                for email in emails:
                    instance = Newsletter.objects.get(email=email)
                    token = instance.token
                    url = f"<a href='{domainname}/news/newsletters/unsubsribe/{token}'>Unsubscribe</a>"
                    bodyandurl = form.cleaned_data["content"] + url

                    try:
                        msg = EmailMessage(
                            subject=form.cleaned_data["subject"],
                            body=bodyandurl,
                            from_email=settings.EMAIL_HOST_USER,
                            to=list(email.split(" ")),
                        )
                        if form.cleaned_data["file_attachment"] != None:
                            msg.attach(
                                form.cleaned_data["file_attachment"].name,
                                form.cleaned_data["file_attachment"].file.getvalue(),
                            )
                        msg.send()
                    except (ConnectionError, SMTPException):
                        messages.error(self.request, "The email settings are not valid.")
                        return HttpResponseRedirect(self.next)
        return HttpResponseRedirect(self.next)


class NewsletterUnsubscribeView(View):
    def get(self, request, token):
        try:
            user = Newsletter.objects.get(token=token)
            user.delete()
            messages.success(self.request, "You have been unsubscribed from the newsletter.")
        except:
            messages.error(self.request, "The token is not valid.")
        return HttpResponseRedirect("/")