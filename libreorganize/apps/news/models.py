from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify

from apps.accounts.models import Account


class Announcement(models.Model):
    slug = models.SlugField(max_length=64, unique=True)
    title = models.CharField(max_length=64)
    content = models.TextField()
    author = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created_on"]
        default_permissions = ()
        permissions = (
            ("view_announcements", "View announcements"),
            ("manage_announcements", "Manage announcements"),
        )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)


class Newsletter(models.Model):
    email = models.EmailField(
        unique=True, error_messages={"unique": "This email is already subscribed to the newsletters."}
    )
    full_name = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    language = models.CharField(max_length=16, choices=settings.LANGUAGES, default="en")
    token = models.CharField(max_length=32)

    class Meta:
        default_permissions = ()
        permissions = (
            ("view_newsletters", "View newsletters"),
            ("manage_newsletters", "Manage newsletters"),
        )

    def __str__(self):
        return self.email
