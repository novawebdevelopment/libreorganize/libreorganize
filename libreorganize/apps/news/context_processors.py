from apps.news.models import Announcement


def announcements_processor(request):
    announcements = Announcement.objects.all().order_by("-created_on")[:5]
    return {"announcements": announcements}
