from django.test import TestCase

from apps.news.apps import NewsConfig


class NewsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(NewsConfig.name, "apps.news")
