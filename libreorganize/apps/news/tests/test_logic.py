from django.test import TestCase

from apps.accounts.models import Account
from apps.news.models import Newsletter, Announcement


class TestListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and announcements"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.announcement1 = Announcement.objects.get(pk=1)
        self.announcement2 = Announcement.objects.get(pk=2)


class TestCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and announcements"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create_successful(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {
                "title": "This is a test",
                "content": "abc abc testing",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.post("/news/announcements/create/", follow=True)
        self.assertTemplateUsed(response, template_name="news/announcements/create.html")

    def test_invalid_url(self):
        """Test invalid URL"""
        self.client.force_login(self.superuser)
        response = self.client.get("/news/announcements/inexistent-announcement/edit/", follow=True)
        self.assertContains(response, "The announcement doesn&#x27;t exist.")
        self.assertRedirects(response, "/")

        """More than three char in title"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "s", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The title must have at least 3 characters.")

        """Test invalid titles"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "create", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "&quot;Create&quot; is not a valid title.")

    def test_taken_title(self):
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "Test", "content": "This field is required."},
            follow=True,
        )
        self.assertContains(response, "An announcement with a similar title already exists.")

    def test_incorrect_title(self):
        """Check create unsuccessful (title)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "This field is required.")

    def test_incorrect_content(self):
        """Check create unsuccessful (content)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "News", "content": ""},
            follow=True,
        )
        self.assertContains(response, "This field is required.")


class TestDetailView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_announcement_detail_page(self):
        """Bring to Detail Page"""
        self.client.force_login(self.superuser)
        response = self.client.get("/news/announcements/test/", follow=True)
        self.assertContains(response, "title")


class TestEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "test", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        response = self.client.post(
            "/news/announcements/test/edit/",
            {
                "title": "This is an edited test",
                "content": "abc abc testing",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully edited.")

    def test_incorrect_announcement(self):
        """Check edit unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {"title": "testing", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        response = self.client.get("/news/announcements/inexistent-announcement/edit/", follow=True)
        self.assertContains(response, "The announcement doesn&#x27;t exist.")


class TestDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/announcements/create/",
            {
                "title": "This is a test",
                "content": "abc abc testing",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")
        response = self.client.post(
            "/news/announcements/this-is-a-test/delete/",
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully deleted.")

    def test_incorrect_delete(self):
        """Check delete unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/news/announcements/inexistent-wiki/delete/", follow=True)
        self.assertContains(response, "The announcement doesn&#x27;t exist.")


class TestNewsletterList(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_list(self):
        """Test list"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        response = self.client.get("/news/newsletters/", follow=True)
        self.assertContains(response, "Joe Smith")


class TestNewsletterCreate(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter user has been successfully added.")

    def test_invalid_name(self):
        """Test invalid full name"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/newsletters/create/",
            {"full_name": "^#*$*%#", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "Enter a valid full name.")

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.post("/news/newsletters/create/", follow=True)
        self.assertTemplateUsed(response, template_name="news/newsletters/create.html")


class TestNewsletterEdit(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter user has been successfully added.")
        response = self.client.post(
            "/news/newsletters/1/edit/?next=/news/newsletters/",
            {"full_name": "Joe Smith Edit", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter has been successfully edited.")


class TestNewsletterDelete(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/news/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter user has been successfully added.")
        response = self.client.post(
            "/news/newsletters/1/delete/?next=/news/newsletters/",
            follow=True,
        )
        self.assertContains(response, "The newsletter has been successfully deleted.")
