from django.test import TestCase
from django.urls import resolve


class AnnouncementsUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/news/announcements/").view_name, "news:announcements_list")

    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/news/announcements/create/").view_name, "news:announcements_create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/news/announcements/test/").view_name, "news:announcements_detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/news/announcements/test/edit/").view_name, "news:announcements_edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/news/announcements/test/delete/").view_name, "news:announcements_delete")


class NewsletterUrls(TestCase):
    def test_list(self):
        """Test newsletter list"""
        self.assertEqual(resolve("/news/newsletters/").view_name, "news:newsletters_list")

    def test_send(self):
        """Test newsletter send"""
        self.assertEqual(resolve("/news/newsletters/send/").view_name, "news:newsletters_send")

    def test_create(self):
        """Test newsletter create"""
        self.assertEqual(resolve("/news/newsletters/create/").view_name, "news:newsletters_create")

    def test_edit(self):
        """Test newsletter edit"""
        self.assertEqual(resolve("/news/newsletters/1/edit/").view_name, "news:newsletters_edit")

    def test_delete(self):
        """Test newsletter edit"""
        self.assertEqual(resolve("/news/newsletters/1/delete/").view_name, "news:newsletters_delete")