from django.urls import path

from apps.news import views

app_name = "apps.news"

urlpatterns = [
    # Require VIEW permissions
    path("newsletters/", views.NewsletterListView.as_view(), name="newsletters_list"),
    path("announcements/", views.AnnouncementListView.as_view(), name="announcements_list"),
    # Require MANAGE permissions
    path("newsletters/send/", views.NewsletterSendMailView.as_view(), name="newsletters_send"),
    path("newsletters/create/", views.NewsletterCreateView.as_view(), name="newsletters_create"),
    path("newsletters/<int:pk>/edit/", views.NewsletterEditView.as_view(), name="newsletters_edit"),
    path("newsletters/<int:pk>/delete/", views.NewsletterDeleteView.as_view(), name="newsletters_delete"),
    path("announcements/create/", views.AnnouncementCreateView.as_view(), name="announcements_create"),
    path("announcements/<slug:slug>/edit/", views.AnnouncementEditView.as_view(), name="announcements_edit"),
    path("announcements/<slug:slug>/delete/", views.AnnouncementDeleteView.as_view(), name="announcements_delete"),
    # Require NO permissions
    path("announcements/<slug:slug>/", views.AnnouncementDetailView.as_view(), name="announcements_detail"),
    path("newsletters/unsubscribe/<token>/", views.NewsletterUnsubscribeView.as_view(), name="newsletters_unsubscribe"),
]
