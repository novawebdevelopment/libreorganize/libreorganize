import re

from django import forms
from django.conf import settings
from django.template.defaultfilters import slugify
from django.utils.translation import gettext_lazy as _
from ckeditor.fields import CKEditorWidget

from apps.news.models import Announcement, Newsletter


class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = Announcement
        fields = ("title", "content")
        widgets = {"content": CKEditorWidget()}

    def clean_title(self):
        try:
            announcement = Announcement.objects.get(slug=slugify(self.cleaned_data["title"]))
            if announcement == self.instance:
                raise Announcement.DoesNotExist
            raise forms.ValidationError(_("An announcement with a similar title already exists."))
        except Announcement.DoesNotExist:
            if self.cleaned_data["title"].lower() == "create":
                raise forms.ValidationError(_('"Create" is not a valid title.'))
            if len(self.cleaned_data["title"]) < 3:
                raise forms.ValidationError(_("The title must have at least 3 characters."))
            return self.cleaned_data["title"]


class NewsletterForm(forms.ModelForm):
    email = forms.EmailField(label=_("Email"))

    class Meta:
        model = Newsletter
        fields = ["full_name", "email", "language"]
        labels = {"full_name": "Full Name", "language": "Language"}

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_full_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["full_name"]):
            raise forms.ValidationError("Enter a valid full name.")
        return self.cleaned_data["full_name"]


class SendMailForm(forms.Form):
    languages = forms.CharField(
        widget=forms.Select(
            choices=settings.LANGUAGES
            + (
                (
                    "all",
                    _("Everyone"),
                ),
            )
        )
    )
    subject = forms.CharField()
    content = forms.CharField(widget=forms.Textarea)
    file_attachment = forms.FileField(required=False)

    def clean_file_attachment(self):
        if self.cleaned_data["file_attachment"]:
            if "." not in self.cleaned_data["file_attachment"].name:
                raise forms.ValidationError("The file does not have an extension.")
        return self.cleaned_data["file_attachment"]
