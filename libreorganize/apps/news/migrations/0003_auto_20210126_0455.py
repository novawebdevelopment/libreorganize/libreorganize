# Generated by Django 2.2.17 on 2021-01-26 04:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20210116_0328'),
    ]

    operations = [
        migrations.AlterField(
            model_name='announcement',
            name='slug',
            field=models.SlugField(max_length=64, unique=True),
        ),
        migrations.AlterField(
            model_name='announcement',
            name='title',
            field=models.CharField(max_length=64),
        ),
    ]
