from django.test import TestCase
from django.urls import resolve


class WikisUrlsCase(TestCase):
    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/wikis/create/").view_name, "wikis:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/wikis/test/").view_name, "wikis:view")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/wikis/test/edit/").view_name, "wikis:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/wikis/test/delete/").view_name, "wikis:delete")
