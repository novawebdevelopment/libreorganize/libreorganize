from django import forms
from django.template.defaultfilters import slugify
from django.utils.translation import gettext_lazy as _
from ckeditor.fields import CKEditorWidget

from apps.wikis.models import Wiki


class WikiForm(forms.ModelForm):
    class Meta:
        model = Wiki
        fields = ("title", "content", "groups")
        widgets = {"content": CKEditorWidget(), "groups": forms.CheckboxSelectMultiple}

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.title == "Home":
            self.fields["title"].disabled = True

    def clean_title(self):
        try:
            wiki = Wiki.objects.get(slug=slugify(self.cleaned_data["title"]))
            if wiki == self.instance:
                raise Wiki.DoesNotExist
            raise forms.ValidationError(_("The title is similiar with the one of another wiki."))
        except Wiki.DoesNotExist:
            if self.cleaned_data["title"].lower() == "create":
                raise forms.ValidationError(_('"Create" is not a valid title.'))
            if len(self.cleaned_data["title"]) < 3:
                raise forms.ValidationError(_("The title must have at least 3 characters."))
            return self.cleaned_data["title"]
