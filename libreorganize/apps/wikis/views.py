from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from apps.wikis.forms import WikiForm
from apps.wikis.models import Wiki
from core.views import CoreCreateView, CoreDeleteView, CoreDetailView, CoreEditView, CoreListView


class DetailView(CoreDetailView):
    personal = False
    permission = "wikis.view_wikis"
    model = Wiki
    template_name = "wikis/detail.html"

    def get(self, request):
        self.wiki_list = list(Wiki.objects.all())
        for wiki in self.wiki_list:
            if "[[" + wiki.title + "]]" in self.wiki.content:
                self.wiki.content = self.wiki.content.replace(
                    "[[" + wiki.title + "]]", f"<a href=/wikis/{wiki.slug}/>{wiki.title}</a>"
                )
        self.wiki.content = mark_safe(self.wiki.content)

        if self.model.__name__.lower() != "account" and hasattr(self.model, "groups"):
            objects = self.model.objects.filter(groups=None)
            if self.request.user.is_superuser:
                objects = self.model.objects.all()
            else:
                for group in self.request.user.groups.all():
                    objects = objects | self.model.objects.filter(groups=group)
                objects = objects.distinct()
        else:
            objects = self.model.objects.all()
        self.wiki_list = objects.exclude(slug="home")
        return render(
            request=request, template_name="wikis/detail.html", context={"wiki": self.wiki, "wiki_list": self.wiki_list}
        )


class CreateView(CoreCreateView):
    personal = False
    permission = "wikis.manage_wikis"
    model = Wiki
    form_name = WikiForm
    template_name = "wikis/create.html"


class EditView(CoreEditView):
    personal = False
    permission = "wikis.manage_wikis"
    model = Wiki
    form_name = WikiForm
    template_name = "wikis/edit.html"


class DeleteView(CoreDeleteView):
    personal = False
    permission = "wikis.manage_wikis"
    model = Wiki
    template_name = "wikis/delete.html"

    def get(self, request):
        if self.wiki.title == "Home":
            messages.error(request, _("You cannot delete this wiki page."))
            return HttpResponseRedirect(reverse("wikis:home"))
        return render(request=request, template_name="wikis/delete.html")