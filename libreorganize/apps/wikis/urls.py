from django.urls import path
from django.views.generic import RedirectView

from apps.wikis import views

app_name = "apps.wikis"

urlpatterns = [
    # Require MANAGE permissions
    path("create/", views.CreateView.as_view(), name="create"),
    path("<slug:slug>/edit/", views.EditView.as_view(), name="edit"),
    path("<slug:slug>/delete/", views.DeleteView.as_view(), name="delete"),
    # Require VIEW permissions
    path("", RedirectView.as_view(url="/wikis/home/"), name="home"),
    path("<slug:slug>/", views.DetailView.as_view(), name="view"),
]
