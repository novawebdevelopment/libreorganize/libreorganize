��          �      �           	     (     E  *   L     w     ~     �     �     �     �     �  	   �     �     �  :   �  *   &  '   Q  '   y  &   �     �     �  	   �  B  �  !   8  %   Z     �  1   �     �  
   �     �     �     �     �                    &  E   9  ,         �  &   �  $   �          7     ?                                                                                        	            
    "Create" is not a valid title. "Logs" is not a valid title. Action Are you sure you want to delete this wiki? Author Create Wiki Date Modified Delete Delete Wiki Deleted Account Deleted Wiki Edit Wiki Submit Table of Contents The title must contain only URL-safe characters or spaces. The title must have at least 3 characters. The wiki has been successfully created. The wiki has been successfully deleted. The wiki has been successfully edited. This action is irreversible. Title Wiki Logs Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 "Crear" no es un título válido. "Registros" no es un título válido. Acción ¿Estás seguro de que deseas eliminar esta wiki? Autor Crear Wiki Fecha Modificada Eliminar Eliminar Wiki Cuenta Eliminada Wiki Eliminado Editar Wiki Enviar Tabla de Contenido El título debe contener solo caracteres o espacios seguros para URL. El título debe tener al menos 3 caracteres. La wiki se ha creado con éxito. La wiki se ha eliminado correctamente. La wiki se ha editado correctamente. Esta acción es irreversible. Título Registros Wiki 