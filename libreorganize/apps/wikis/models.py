from django.http import Http404
from django.db import models
from django.forms import ValidationError
from django.template.defaultfilters import slugify

from apps.accounts.models import Account, Group


class Wiki(models.Model):
    slug = models.SlugField(max_length=64, unique=True)
    title = models.CharField(max_length=64)
    content = models.TextField()
    groups = models.ManyToManyField(Group, related_name="wikis", blank=True)

    class Meta:
        default_permissions = ()
        permissions = (
            ("view_wikis", "View wikis"),
            ("manage_wikis", "Manage wikis"),
        )

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)
